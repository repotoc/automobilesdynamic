﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;
using System.Configuration;
using System.IO;
using System.Windows.Media.Animation;
using System.Collections.ObjectModel;
using Automobiles.Admin;
using Automobiles.UserControls;
using Automobiles.UserControls.AlertControls;
using System.Windows.Threading;
using TOCCatalogDB;
using TOCCatalogDB.Models;
using TOCCatalog.Contracts;
using WPFTabTip;
using System.Reflection;

namespace Automobiles
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public TOCCatalogDB.DBFolder.TOCCatalogDbContext DBContext;
        public static int zIndexValue = 999;
        public static int ivRenTrsf;
        public static int ivRenTrsfX, ivRenTrsfY;
        public Product currentProduct { get; set; }
        public FilterDetail currentFilterDetail { get; set; }
        private DispatcherTimer idleTimer, autoSyncTimer;
        DispatcherTimer timerImagesChange;
        int destinationImageNumber = 0, currentImageNumber = 0;
        bool isOverviewItemDisplayed = true;
        bool IsFloatingBtnSelected = false;
        private Configuration config;
        CarUc car360 = new CarUc();
        Category category = new Category();
        List<FilterTitle> lstFilterTitle = new List<FilterTitle>();

        public MainWindow()
        {
            InitializeComponent();
            ivRenTrsf = 10;
        }

        private int SetZIndexValue()    //increment ZIndex value method
        {
            return zIndexValue++;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            TabTipAutomation.BindTo<TextBox>();
            TabTipAutomation.BindTo<PasswordBox>();

            List<Category> categ = Apps.tocCatalogContext.Category.ToList();
            LoadAudioControl();
            #region Manipulation Events
            this.ManipulationStarting -= HomePage_ManipulationStarting;
            this.ManipulationDelta -= HomePage_ManipulationDelta;
            this.ManipulationStarting += HomePage_ManipulationStarting;
            this.ManipulationDelta += HomePage_ManipulationDelta;
            #endregion
            LoadSlideshow();
            Logger.Info("Completed");
        }

        #region ManipulationEvents

        ManipulationModes currentMode = ManipulationModes.All;
        private void HomePage_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
            {
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);
            }

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }
        private void HomePage_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            if (element != null)
            {
                MatrixTransform xform = element.RenderTransform as MatrixTransform;
                Matrix matrix = xform.Matrix;
                ManipulationDelta delta = args.DeltaManipulation;
                Point center = args.ManipulationOrigin;
                matrix.Translate(-center.X, -center.Y);
                matrix.Scale(delta.Scale.X, delta.Scale.Y);
                matrix.Translate(center.X, center.Y);
                matrix.Translate(delta.Translation.X, delta.Translation.Y);
                xform.Matrix = matrix;
            }
            args.Handled = true;
            base.OnManipulationDelta(args);
        }
        #endregion

        #region Slideshow
        private void LoadSlideshow()
        {
            Logger.Info("Initiated");
            Apps.lstCompareProducts.Clear();
            btnFloatingButton.Visibility = Visibility.Collapsed;
            btnCompare.Visibility = Visibility.Collapsed;
            btnCompare.Content = "0";
            currentProduct = null;
            ivRenTrsf = 10;
            zIndexValue = 999;
            GridCarPanel.Children.Clear();
            GridCarDetailsPanel.Children.Clear();
            GridSlideshowPanel.Children.Clear();
            GridFloatingContentPanel.Children.Clear();
            GridFloatingPopUpPanel.Children.Clear();
            GridFilterFooterPanel.Children.Clear();
            GridPopUpPanel.Children.Clear();

            VideoSlideshowUc videoSlideshow = new VideoSlideshowUc();
            GridSlideshowPanel.Children.Add(videoSlideshow);
            videoSlideshow.EventCloseVideoSlideshow += videoSlideshow_EventCloseVideoSlideshow;
            videoSlideshow.EventOpenAdminLogin += videoSlideshow_EventOpenAdminLogin;
            MEAudio.Pause();
            GridAudioPanel.Visibility = Visibility.Collapsed;

            Logger.Info("Completed");
        }

        void videoSlideshow_EventOpenAdminLogin(object sender, EventArgs e)
        {
            AdminLogin();
        }

        void videoSlideshow_EventCloseVideoSlideshow(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            VideoSlideshowUc videoSlideshow = (VideoSlideshowUc)sender;
            GridSlideshowPanel.Children.Remove(videoSlideshow);
            LoadHome();
            Logger.Info("Completed");
        }

        void slideShow_EventCloseSlideshowUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            SlideShowUc slideShow = (SlideShowUc)sender;
            GridSlideshowPanel.Children.Remove(slideShow);
            LoadHome();
            Logger.Info("Completed");
        }
        #endregion

        private void LoadHome()
        {
            Logger.Info("Initiated");
            btnFloatingButton.Visibility = Visibility.Collapsed;
            btnCompare.Visibility = Visibility.Collapsed;
            CloseCarControls();

            //MEAudio.Source = new Uri(audioCtrl.AudioItem.AudioLocation);
            //MEAudio.Play();
            ////MEAudio.Position = new TimeSpan(0, 0, 32);
            //btnAudioVolume.Visibility = Visibility.Visible;
            //GridAudioPanel.Visibility = Visibility.Visible;

            //lstProducts = Apps.tocCatalogContext.Product.Where(d => d.LstLogin.ToList().Any(f => f.UserName.Equals("myfadmin"))).ToList();
            if (category != null)
            {
                category = Apps.tocCatalogContext.Category.ToList().Where(c => c.Name.Equals("Audi")).FirstOrDefault();
                CarMenuUc carMenu = new CarMenuUc();
                GridCarPanel.Background = new SolidColorBrush(Colors.Transparent);
                carMenu.lstProducts = category.LstProduct.ToList();
                GridCarPanel.Children.Add(carMenu);
                carMenu.EventOpenCarUc += carMenu_EventOpenCarUc;
            }
            Logger.Info("Completed");
        }
        void carMenu_EventOpenCarUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            Product selectedProduct = sender as Product;
            currentProduct = selectedProduct;
            LoadProductColorWise(currentProduct);

            //CloseCarMenuUC();
            //btnCompare.Visibility = Visibility.Visible;
            //btnFloatingButton.Visibility = Visibility.Visible;

            ////load car control
            //CarUc carControl = new CarUc();
            //carControl.product = selectedProduct;
            //GridCarDetailsPanel.Children.Add(carControl);
            //if (currentProduct != null)
            //{
            //    FilterUc filters = new FilterUc();
            //    filters.prodFilter = currentProduct.LstMainFilter.ToList().Where(c=>c.IsActive.Equals(true)).ToList(); // need to check data exists and then bind also active or not
            //    Panel.SetZIndex(filters, SetZIndexValue());
            //    GridCarDetailsPanel.Children.Add(filters);
            //    filters.EventColorControl += filters_EventColorControl;
            //    filters.EventOverviewControl += filters_EventOverviewControl;
            //    filters.EventBrochureControl += filters_EventBrochureControl;
            //    //filters.EventVariantsControl += filters_EventVariantsControl;
            //    filters.EventGalleryControl += filters_EventGalleryControl;
            //}
            Logger.Info("Completed");
        }

        //LoadProductColorWise -> list of products colors wise display here.
        private void LoadProductColorWise(Product currentProduct)
        {
            Logger.Info("Initiated");
            CloseCarMenuUC();
            btnCompare.Visibility = Visibility.Visible;
            btnFloatingButton.Visibility = Visibility.Visible;
            FilterTitle fTitle = currentProduct.LstMainFilter.ToList().Where(c => c.Name.Equals("Colors") && c.IsActive.Equals(true)).FirstOrDefault();
            if (fTitle != null && fTitle.LstFilterDetail != null && fTitle.LstFilterDetail.Count > 0)
            {
                ProductColorsUc prodColorWise = new ProductColorsUc();
                prodColorWise.lstColorFilterDetail = fTitle.LstFilterDetail.Where(c=> currentProduct.LstFilterDetail.Select(d=>d.SNo).Contains(c.SNo)).ToList();
                GridCarDetailsPanel.Children.Add(prodColorWise);
                prodColorWise.EventOpenFilterDetailItem += prodColorWise_EventOpenFilterDetailItem;
            }
            Logger.Info("Completed");
        }

        //prodColorWise_EventOpenFilterDetailItem -> raised on selection of particular color product.
        //now get the LinkedFilterTitle and bind to LinkedFilterTitleUc this will be filters on left side of page.
        //load basic details of the particular color as well.
        void prodColorWise_EventOpenFilterDetailItem(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            FilterDetail fDetailItem = sender as FilterDetail;
            if (fDetailItem != null)
            {
                ClearProducColorUc();
                currentFilterDetail = fDetailItem;  //save at currentFilterDetail
                LoadLinkedFilterTitleUc(fDetailItem); //filters on left side
                //load basic detail like car 360 degree images and name.
                LoadProduct360View();
            }
            Logger.Info("Completed");
        }

        private void LoadProduct360View()
        {
            Product360View productView = new Product360View();
            productView.currentProduct = currentProduct;
            GridCarDetailsPanel.Children.Add(productView);
        }

        #region LinkedFilterTitleControls
        private void LoadLinkedFilterTitleUc(FilterDetail filterDetail)
        {
            Logger.Info("Initiated");
            ClearFilterUc();
            if (CheckLinkedFilterTitleExists(filterDetail))
            {
                ClearLinkedFilterTitleUc();
                LinkedFilterTitleUc lnkFTitle = new LinkedFilterTitleUc();
                lnkFTitle.filterDetail = filterDetail;
                Panel.SetZIndex(lnkFTitle, SetZIndexValue());
                GridCarDetailsPanel.Children.Add(lnkFTitle);
                lnkFTitle.EventLFTGalleryControl += lnkFTitle_EventLFTGalleryControl;
                lnkFTitle.EventLFTBrochureControl += lnkFTitle_EventLFTBrochureControl;
                lnkFTitle.EventLFTOverviewControl += lnkFTitle_EventLFTOverviewControl;
                lnkFTitle.EventLFTVariantsControl += lnkFTitle_EventLFTVariantsControl;
            }
            Logger.Info("Completed");
        }
        private bool CheckLinkedFilterTitleExists(FilterDetail filterDetail)
        {
            Logger.Info("Initiated");
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is LinkedFilterTitleUc)
                {
                    LinkedFilterTitleUc lnkFilterTitleUc = item as LinkedFilterTitleUc;
                    if (filterDetail.SNo.Equals(lnkFilterTitleUc.filterDetail.SNo))
                    {
                        return false;
                    }
                }
            }
            Logger.Info("Completed");
            return true;
        }

        #region FuelVariantControls
        void lnkFTitle_EventLFTVariantsControl(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ClearPopupPanelControls();
            LinkedFilterTitle lnkdVariantFilterTitle = sender as LinkedFilterTitle;
            if (lnkdVariantFilterTitle != null && currentProduct != null && currentProduct.LstLinkingFilterDetail != null && currentProduct.LstLinkingFilterDetail.Count > 0)
            {
                List<LinkedFilterDetail> lstLnkdFilterDetail = currentProduct.LstLinkingFilterDetail.Where(c => c.LinkedFilter.SNo.Equals(lnkdVariantFilterTitle.SNo)).ToList();
                LoadFuelVariantControl(lstLnkdFilterDetail);
            }
            Logger.Info("Completed");
        }

        private void LoadFuelVariantControl(List<LinkedFilterDetail> lstLnkdFilterDetail)
        {
            Logger.Info("Initiated");
            if (CheckFuelVariantControlExists())
            {
                FuelVariantControl fuelControl = new FuelVariantControl();
                fuelControl.lstLnkdFilterDetail = lstLnkdFilterDetail;
                GridCarDetailsPanel.Children.Add(fuelControl);
                fuelControl.EventSelectedFuelVariantItem += fuelControl_EventSelectedFuelVariantItem;
            }
            Logger.Info("Completed");
        }

        private bool CheckFuelVariantControlExists()
        {
            Logger.Info("Initiated");
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is FuelVariantControl) { return false; }
            }
            Logger.Info("Completed");
            return true;
        }

        void fuelControl_EventSelectedFuelVariantItem(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            LinkedFilterDetail lnkdFilterDetail = sender as LinkedFilterDetail;
            if (lnkdFilterDetail != null)
            {
                if (CheckVariantDetailsExists())
                {
                    VariantDetailUc variant = new VariantDetailUc();
                    variant.lnkFilterDetail = lnkdFilterDetail;
                    variant.currentProduct = currentProduct;
                    GridCarDetailsPanel.Children.Add(variant);
                }
                else
                {
                    
                }
            }
            Logger.Info("Completed");
        }

        private bool CheckVariantDetailsExists()
        {
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is VariantDetailUc)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        void lnkFTitle_EventLFTOverviewControl(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ClearPopupPanelControls();
            //load overview control
            Logger.Info("Completed");
        }

        void lnkFTitle_EventLFTBrochureControl(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ClearPopupPanelControls();
            LinkedFilterTitle lnkdVariantFilterTitle = sender as LinkedFilterTitle;
            if (lnkdVariantFilterTitle != null && currentProduct != null && currentProduct.LstLinkingFilterDetail != null && currentProduct.LstLinkingFilterDetail.Count > 0)
            {
                LinkedFilterDetail lnkdFilterDetail = currentProduct.LstLinkingFilterDetail.Where(c => c.LinkedFilter.SNo.Equals(lnkdVariantFilterTitle.SNo)).FirstOrDefault();
                if (lnkdFilterDetail != null)
                {
                    LoadBrochureView(lnkdFilterDetail.LstLinkedFilterDetailData.ToList());
                }
                //LoadFuelVariantControl(lstLnkdFilterDetail);
            }
            Logger.Info("Completed");
            //load brochure control
        }

        private void LoadBrochureView(List<LinkedFilterDetailData> list)
        {
            Logger.Info("Initiated");
            BrochureViewUc brochure = new BrochureViewUc();
            brochure.lstLnkdFilterDetailData = list;
            brochure.IsManipulationEnabled = true;
            brochure.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsfX, ivRenTrsfY);
            brochure.BringIntoView();
            Panel.SetZIndex(brochure, SetZIndexValue());
            GridPopUpPanel.Children.Add(brochure);
            brochure.EventCloseBrochureView += brochure_EventCloseBrochureView;
            Logger.Info("Completed");
        }

        void brochure_EventCloseBrochureView(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            BrochureViewUc brochure = (BrochureViewUc)sender;
            GridPopUpPanel.Children.Remove(brochure);
            Logger.Info("Completed");
        }
        #endregion

        #region LinkedFilterDetailControls
        void lnkFTitle_EventLFTGalleryControl(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ClearPopupPanelControls();            
            LinkedFilterTitle lnkFilterTitle = sender as LinkedFilterTitle;
            long lnkFilterID = lnkFilterTitle.SNo;
            List<LinkedFilterDetail> lnkFilterDetail = currentProduct.LstLinkingFilterDetail.Where(c => c.LinkedFilter.SNo.Equals(lnkFilterID)).ToList();
            lnkFilterDetail.Add(new LinkedFilterDetail() { Name = "Back", SNo = lnkFilterDetail.Count + 1, Version = lnkFilterDetail.Count + 1, ID = lnkFilterDetail.Count + 1, IsActive = true });
            ClearLinkedFilterTitleUc();
            LinkedFilterDetailUc lnkedFilterDetail = new LinkedFilterDetailUc();
            lnkedFilterDetail.lnkedFilterDetail = lnkFilterDetail;
            Panel.SetZIndex(lnkedFilterDetail, SetZIndexValue());
            GridCarDetailsPanel.Children.Add(lnkedFilterDetail);
            lnkedFilterDetail.EventLinkedFilterTitleUc += lnkedFilterDetail_EventLinkedFilterTitleUc;
            lnkedFilterDetail.EventLoadGalleryImages += lnkedFilterDetail_EventLoadGalleryImages;
            Logger.Info("Completed");
        }
        void lnkedFilterDetail_EventLinkedFilterTitleUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            if (currentFilterDetail != null)
            {
                ClearLinkedFilterDetailUc();
                LoadLinkedFilterTitleUc(currentFilterDetail);
            }
            Logger.Info("Completed");
        }
        void lnkedFilterDetail_EventLoadGalleryImages(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            LinkedFilterDetail lnkFilterDetail = sender as LinkedFilterDetail;
            if (lnkFilterDetail != null && lnkFilterDetail.LstLinkedFilterDetailData != null && lnkFilterDetail.LstLinkedFilterDetailData.Count > 0)
            {
                lnkFilterDetail.LstLinkedFilterDetailData.ToList().ForEach(linkedFilterDetailDataItem =>
                {
                    LoadLinkedFilterDetailDataImage(linkedFilterDetailDataItem);
                });
            }
            Logger.Info("Completed");
        }
        private void LoadLinkedFilterDetailDataImage(LinkedFilterDetailData lnkdFilterDetailData)
        {
            Logger.Info("Initiated");
            if (CheckLinkedFilterDataExists(lnkdFilterDetailData))
            {
                ivRenTrsfX = new Random().Next(0, 1000);
                ivRenTrsfY = new Random().Next(0, 300);
                LinkedFilterDetailDataImageView image = new LinkedFilterDetailDataImageView();
                image.linkedFilterDetailData = lnkdFilterDetailData;
                image.IsManipulationEnabled = true;
                image.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsfX, ivRenTrsfY);
                image.BringIntoView();
                Panel.SetZIndex(image, SetZIndexValue());
                GridPopUpPanel.Children.Add(image);
                image.EventCloseLinkedFilterDetailDataImageView += image_EventCloseLinkedFilterDetailDataImageView;
                image.EventErrorLinkedFilterDetailDataImageView += image_EventErrorLinkedFilterDetailDataImageView;
            }
            Logger.Info("Completed");
        }

        void image_EventErrorLinkedFilterDetailDataImageView(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = sender as string;
            ErrorAlert(msg);
            Logger.Info("Completed");
        }

        void image_EventCloseLinkedFilterDetailDataImageView(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            LinkedFilterDetailDataImageView image = (LinkedFilterDetailDataImageView)sender;
            GridPopUpPanel.Children.Remove(image);
            Logger.Info("Completed");
        }

        private bool CheckLinkedFilterDataExists(LinkedFilterDetailData lnkdFilterDetailData)
        {
            foreach (UIElement item in GridPopUpPanel.Children)
            {
                if (item is LinkedFilterDetailDataImageView)
                {
                    LinkedFilterDetailDataImageView imgView = item as LinkedFilterDetailDataImageView;
                    if (imgView.linkedFilterDetailData.SNo.Equals(lnkdFilterDetailData.SNo))
                    {
                        ivRenTrsfX = new Random().Next(350, 600);
                        ivRenTrsfY = new Random().Next(0, 100);
                        imgView.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsfX, ivRenTrsfY);
                        imgView.BringIntoView();
                        Panel.SetZIndex(imgView, SetZIndexValue());
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion

        #region ClearControlSection
        private void ClearLinkedFilterTitleUc()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is LinkedFilterTitleUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c); //remove
            });
        }
        private void ClearLinkedFilterDetailUc()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is LinkedFilterDetailUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c); //remove
            });
        }

        private void ClearFilterUc()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is FilterUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c); //remove
            });
        }

        private void ClearProducColorUc()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is ProductColorsUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c); //remove
            });
        } 
        #endregion

        #region Gallery
        void filters_EventGalleryControl(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<FilterDetail> fDetail = sender as List<FilterDetail>;
            if (fDetail != null && currentProduct != null && currentProduct.LstFilterDetail != null && currentProduct.LstFilterDetail.Count > 0)
            {
                //VariantsUc variant = new VariantsUc();                
                GridFilterFooterPanel.Children.Clear();
                GalleryUc gallery = new GalleryUc();
                gallery.lstFilterDetail = fDetail.Where(c => currentProduct.LstFilterDetail.Select(d => d.SNo).Contains(c.SNo)).ToList();
                GridFilterFooterPanel.Children.Add(gallery);
                gallery.EventOpenGalleryImages += gallery_EventOpenGalleryImages;
                gallery.EventCloseImageControl += gallery_EventCloseImageControl;
            }
            Logger.Info("Completed");
        }

        void gallery_EventCloseImageControl(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseImageViewUC();
            Logger.Info("Completed");
        }

        void gallery_EventOpenGalleryImages(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<FilterDetailData> lstGalleryItems = sender as List<FilterDetailData>;
            foreach (FilterDetailData filterData in lstGalleryItems)
            {
                LoadImage(filterData);
            }
            Logger.Info("Completed");
        }

        #endregion

        #region Variant
        //void filters_EventVariantsControl(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    List<FilterDetail> fDetail = sender as List<FilterDetail>;
        //    if (fDetail != null && currentProduct != null && currentProduct.LstFilterDetail != null && currentProduct.LstFilterDetail.Count > 0)
        //    {
        //        //VariantsUc variant = new VariantsUc();                
        //        VariantsMenuUc variant = new VariantsMenuUc();
        //        variant.lstFilterDetail = fDetail.Where(c => currentProduct.LstFilterDetail.Select(d => d.SNo).Contains(c.SNo)).ToList();
        //        GridCarDetailsPanel.Children.Add(variant);
        //        variant.EventOpenItemDetails += variant_EventOpenItemDetails;
        //    }
        //    Logger.Info("Completed");
        //}
        //void variant_EventOpenItemDetails(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    Product selectedProduct = sender as Product;
        //    ClearVariantDetailsData();
        //    VariantDetailUc vDets = new VariantDetailUc();
        //    vDets.product = selectedProduct;
        //    GridCarDetailsPanel.Children.Add(vDets);
        //    vDets.EventAddToCompare += vDets_EventAddToCompare;
        //    vDets.EventCloseCarDetails += vDets_EventCloseCarDetails;
        //    Logger.Info("Completed");
        //}
        //void vDets_EventAddToCompare(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    Product cmpProduct = sender as Product;
        //    if (!Apps.lstCompareProducts.Any(c => c.SNo.Equals(cmpProduct.SNo)))
        //    {
        //        Apps.lstCompareProducts.Add(cmpProduct);
        //        btnCompare.Content = Apps.lstCompareProducts.Count;
        //        string msg = cmpProduct.Title + " successfully added to compare";
        //        SuccessAlert(msg);
        //    }
        //    else
        //    {
        //        string msg = cmpProduct.Title + "  already exists !!!";
        //        MessageAlert(msg);
        //    }
        //    Logger.Info("Completed");
        //}
        //void vDets_EventCloseCarDetails(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    VariantDetailUc vDets = (VariantDetailUc)sender;
        //    GridCarDetailsPanel.Children.Remove(vDets);
        //    Logger.Info("Completed");
        //}
        #endregion

        #region Brochure
        //void filters_EventBrochureControl(object sender, EventArgs e)
        //{
        //    //Logger.Info("Initiated");
        //    //List<FilterDetail> fDetail = sender as List<FilterDetail>;
        //    //if (fDetail != null && currentProduct != null && currentProduct.LstFilterDetail != null && currentProduct.LstFilterDetail.Count > 0)
        //    //{
        //    //    LoadBrochureView(fDetail.Where(c => currentProduct.LstFilterDetail.Select(d => d.SNo).Contains(c.SNo)).ToList());
        //    //}
        //    //Logger.Info("Completed");
        //}
        //private void LoadBrochureView(List<FilterDetail> fDetail)
        //{
        //    BrochureViewUc brochure = new BrochureViewUc();
        //    brochure.lstFilterDetail = fDetail;
        //    brochure.IsManipulationEnabled = true;
        //    brochure.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsfX, ivRenTrsfY);
        //    brochure.BringIntoView();
        //    Panel.SetZIndex(brochure, SetZIndexValue());
        //    GridPopUpPanel.Children.Add(brochure);
        //    brochure.EventCloseBrochureView += brochure_EventCloseBrochureView;
        //}

        
        #endregion

        #region Overview
        //void filters_EventOverviewControl(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    List<FilterDetail> fDetail = sender as List<FilterDetail>;
        //    if (fDetail != null && currentProduct != null && currentProduct.LstFilterDetail != null && currentProduct.LstFilterDetail.Count > 0)
        //    {
        //        LoadOverview(fDetail.Where(c => currentProduct.LstFilterDetail.Select(d => d.SNo).Contains(c.SNo)).ToList());
        //    }
        //    Logger.Info("Completed");
        //}
        //private void LoadOverview(List<FilterDetail> lstFilterDetail)
        //{
        //    GridFilterFooterPanel.Children.Clear();
        //    OverviewUc overView = new OverviewUc();
        //    overView.lstFilterDetail = lstFilterDetail;
        //    GridFilterFooterPanel.Children.Add(overView);
        //    overView.EventOpenOverviewItem += overView_EventOpenOverviewItem;
        //}

        //void overView_EventOpenOverviewItem(object sender, EventArgs arg)
        //{
        //    if (isOverviewItemDisplayed)
        //    {
        //        Button btn = sender as Button;
        //        //ovItem = btn.DataContext as OverviewItem;
        //        //if (ovItem != null)
        //        //{
        //        //    timerImagesChange = new DispatcherTimer();
        //        //    timerImagesChange.Interval = new TimeSpan(0, 0, 0, 0, 15);
        //        //    timerImagesChange.Tick += new EventHandler(timerImagesChange_Tick);

        //        //    foreach (UIElement item in GridCarDetailsPanel.Children)
        //        //    {
        //        //        if (item is CarUc)
        //        //        {
        //        //            car360 = item as CarUc;
        //        //            destinationImageNumber = Int32.Parse(ovItem.Destination);
        //        //            if (destinationImageNumber <= car360.imagesCount + 1)
        //        //            {
        //        //                string currentImagePath = car360.carImage.Source.ToString();
        //        //                currentImageNumber = Int32.Parse(System.IO.Path.GetFileNameWithoutExtension(currentImagePath.Substring(currentImagePath.LastIndexOf('/') + 1)));
        //        //                isOverviewItemDisplayed = false;
        //        //                timerImagesChange.Start();
        //        //            }
        //        //        }
        //        //    }
        //        //}
        //    }
        //}
        //private void timerImagesChange_Tick(object sender, EventArgs e)
        //{
        //    isOverviewItemDisplayed = false;
        //    //if (destinationImageNumber < currentImageNumber)
        //    //{
        //    //    for (; currentImageNumber >= destinationImageNumber; )
        //    //    {
        //    //        string path = "pack://siteoforigin:,,,/Images/Car360/" + ovItem.CarID + "/" + currentImageNumber + ".png";
        //    //        car360.carImage.Source = new BitmapImage(new Uri(path));
        //    //        currentImageNumber--;
        //    //        break;
        //    //    }
        //    //}
        //    //else if (destinationImageNumber > currentImageNumber)
        //    //{
        //    //    for (; currentImageNumber <= destinationImageNumber; )
        //    //    {
        //    //        string path = "pack://siteoforigin:,,,/Images/Car360/" + ovItem.CarID + "/" + currentImageNumber + ".png";
        //    //        car360.carImage.Source = new BitmapImage(new Uri(path));
        //    //        currentImageNumber++;
        //    //        break;
        //    //    }
        //    //}
        //    //else if (destinationImageNumber == currentImageNumber)
        //    //{
        //    //    string path = ovItem.TempLocation;
        //    //    car360.carImage.Source = new BitmapImage(new Uri(path));
        //    //    if (timerImagesChange != null)
        //    //    {
        //    //        timerImagesChange.Stop();
        //    //        timerImagesChange = null;
        //    //        isOverviewItemDisplayed = true;
        //    //    }
        //    //}
        //}
        #endregion


        //void filters_EventColorControl(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    GridFilterFooterPanel.Children.Clear();
        //    List<FilterDetail> fDetail = sender as List<FilterDetail>;
        //    if (fDetail != null && currentProduct != null && currentProduct.LstFilterDetail != null && currentProduct.LstFilterDetail.Count > 0)
        //    {
        //        ColorsUc color = new ColorsUc();
        //        color.lstFilterDetail = fDetail.Where(c => currentProduct.LstFilterDetail.Select(d => d.SNo).Contains(c.SNo)).ToList();
        //        GridFilterFooterPanel.Children.Add(color);
        //        color.EventSelectedColorItem += color_EventSelectedColorItem;
        //    }
        //    Logger.Info("Completed");
        //}
        //void color_EventSelectedColorItem(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    FilterDetail filterDetail = sender as FilterDetail;
        //    if (filterDetail != null && filterDetail.LstLinkedFilterTitle != null && filterDetail.LstLinkedFilterTitle.Count > 0)
        //    {
        //        currentFilterDetail = filterDetail;
        //        LoadLinkedFilterTitleUc(filterDetail);
        //    }
        //    Logger.Info("Completed");
        //}

        private void LoadAudioControl()
        {
            //Logger.Info("Initiated");
            //audioCtrl = new Audio();
            //string audioFolder = ConfigurationManager.AppSettings["AudioPath"];
            //string audiopath = System.IO.Path.Combine(Apps.strPath, audioFolder);
            //if (File.Exists(audiopath))
            //{
            //    GridAudioPanel.Visibility = Visibility.Visible;
            //    string strInfoAudio = System.IO.File.ReadAllText(audiopath);
            //    audioCtrl = Utilities.GetAudioData(strInfoAudio);
            //    if (audioCtrl != null)
            //    {                    
            //        string path = audioCtrl.AudioItem.AudioLocation;
            //        MEAudio.Source = new System.Uri(path);
            //        MEAudio.Position = new TimeSpan(0, 0, 32);
            //    }
            //}
            //Logger.Info("Completed");
        }

        void filter_EventLoadCarMenu(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            LoadHome();
            Logger.Info("Completed");
        }

        void filter_EventOpenCarImagesUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            Product product = sender as Product;
            string carId = sender as string;
            CloseCarUC();
            CloseBrochureUC();
            CloseVariantsUC();
            CloseGalleryUC();
            CloseOverviewUC();
            CloseImageViewUC();
            LoadCarUc(carId);
            //LoadOverview(product);
            Logger.Info("Completed");
        }

        private void LoadCarUc(string carId)
        {
            //CarImageItem carItem = carImagesData.CarImageItem.Where(c => c.CarID.Equals(carId)).FirstOrDefault();
            //if (carItem != null)
            //{
            //    CarUc carControl = new CarUc();
            //    //carControl.carItem = carItem;
            //    GridCarDetailsPanel.Children.Add(carControl);
            //}
        }

        #region BrochureControls
        void filter_EventOpenBroucherUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseGalleryUC();
            CloseCarUC();
            CloseVariantsUC();
            CloseImageViewUC();
            CloseOverviewUC();

            string carId = sender as string;
            BrochureUc broc = new BrochureUc();
            broc.carID = carId;
            broc.BringIntoView();
            Panel.SetZIndex(broc, SetZIndexValue());
            GridCarDetailsPanel.Children.Add(broc);
            broc.EventOpenBrochureItem += broc_EventOpenBrochureItem;
            Logger.Info("Completed");
        }

        void broc_EventOpenBrochureItem(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string path = sender as string;
            //LoadImage(path);
            Logger.Info("Completed");
        }
        #endregion

        //void filter_EventOpenVariantsUc(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    CloseGalleryUC();
        //    CloseImageViewUC();
        //    CloseBrochureUC();
        //    CloseCarUC();
        //    CloseOverviewUC();

        //    //load carImages control
        //    string carId = sender as string;
        //    LoadCarUc(carId);

        //    //load variants
        //    VariantsUc variant = new VariantsUc();
        //    variant.selectedCarId = carId;
        //    GridCarDetailsPanel.Children.Add(variant);
        //    variant.EventOpenItemDetails += variant_EventOpenItemDetails;

        //    //load colors
        //    //ColorsUc color = new ColorsUc();
        //    ////color.selectedCarId = carId;
        //    //GridCarDetailsPanel.Children.Add(color);
        //    //color.EventShowSelectedCar += color_EventShowSelectedCar;

        //    Logger.Info("Completed");
        //}
        
        private void ClearVariantDetailsData()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is VariantDetailUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c); //remove
            });
        }

        void filter_EventOpenGalleryUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseBrochureUC();
            CloseVariantsUC();
            CloseCarUC();
            CloseOverviewUC();
            CloseImageViewUC();
            //string carId = sender as string;
            //List<GalleryItem> gItem = carGalleryData.GalleryItem.Where(c => c.CarID.Equals(carId)).ToList();
            //if (gItem != null)
            //{
            //    GalleryUc galleryControl = new GalleryUc();
            //    galleryControl.gItem = gItem;
            //    GridCarDetailsPanel.Children.Add(galleryControl);
            //    //galleryControl.EventOpenExteriorImages += galleryControl_EventOpenExteriorImages;
            //    //galleryControl.EventOpenInteriorImages += galleryControl_EventOpenInteriorImages;
            //    //galleryControl.EventCloseImageControl += galleryControl_EventCloseImageControl;
            //}
            Logger.Info("Completed");
        }
        //void galleryControl_EventOpenExteriorImages(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    List<GalleryItem> lstGalleryItems = sender as List<GalleryItem>;
        //    foreach (GalleryItem item in lstGalleryItems)
        //    {
        //        int xPoint = Int32.Parse(item.XAxis);
        //        int yPoint = Int32.Parse(item.YAxis);
        //        LoadGalleryImage(item.ImageLocation, xPoint, yPoint);
        //    }
        //    Logger.Info("Completed");
        //}
        //void galleryControl_EventOpenInteriorImages(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    List<GalleryItem> lstGalleryItems = sender as List<GalleryItem>;
        //    foreach (GalleryItem item in lstGalleryItems)
        //    {
        //        int xPoint = Int32.Parse(item.XAxis);
        //        int yPoint = Int32.Parse(item.YAxis);
        //        LoadGalleryImage(item.ImageLocation, xPoint, yPoint);
        //    }
        //    Logger.Info("Completed");            
        //}

        //void galleryControl_EventCloseImageControl(object sender, EventArgs e)
        //{
        //    Logger.Info("Initiated");
        //    CloseImageViewUC();
        //    Logger.Info("Completed");
        //}

        private void CloseCarControls()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is ColorsUc || item is VariantsUc || item is GalleryUc
                    || item is CarDetailsUc || item is FilterUc
                        || item is BrochureUc || item is VariantDetailUc || item is CarUc || item is OverviewUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });

            ClearPopupPanelControls();
            Logger.Info("Completed");
        }

        private void ClearPopupPanelControls()
        {
            List<int> lstPopUpIndexes = new List<int>();
            foreach (UIElement item in GridPopUpPanel.Children)
            {
                if (item is ImageViewUc)
                {
                    lstPopUpIndexes.Add(GridPopUpPanel.Children.IndexOf(item));
                }
            }
            lstPopUpIndexes = lstPopUpIndexes.OrderByDescending(c => c).ToList();
            lstPopUpIndexes.ForEach(c =>
            {
                GridPopUpPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
        }
        //close car menu control
        private void CloseCarMenuUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarPanel.Children)
            {
                if (item is CarMenuUc)
                {
                    lstIndexes.Add(GridCarPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close Image Control
        private void CloseImageViewUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridPopUpPanel.Children)
            {
                if (item is ImageViewUc)
                {
                    lstIndexes.Add(GridPopUpPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridPopUpPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close Overview Control
        private void CloseOverviewUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is OverviewUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close Brochure Control
        private void CloseBrochureUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is BrochureUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close variants Control which includes VariantsUc, VariantDetailUc and ColorsUc
        private void CloseVariantsUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is VariantsUc || item is VariantDetailUc || item is ColorsUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close variants Control which includes VariantsUc, VariantDetailUc and ColorsUc
        private void CloseGalleryUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is GalleryUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close CarUC user control
        private void CloseCarUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is CarUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }
        private void btnHomeLogo_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            LoadSlideshow();
            Logger.Info("Completed");
        }

        #region ImageView
        private void LoadImage(FilterDetailData filterDetailData)
        {
            Logger.Info("Initiated");
            if (CheckFilterDataExists(filterDetailData))
            {
                ivRenTrsfX = new Random().Next(350, 600);
                ivRenTrsfY = new Random().Next(0, 100);
                ImageViewUc image = new ImageViewUc();
                image.filterDetailData = filterDetailData;
                image.IsManipulationEnabled = true;
                image.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsfX, ivRenTrsfY);
                image.BringIntoView();
                Panel.SetZIndex(image, SetZIndexValue());
                GridPopUpPanel.Children.Add(image);
                image.EventCloseImageViewUc += image_EventCloseImageViewUc;
                image.EventErrorMessage += image_EventErrorMessage;

            }
            Logger.Info("Completed");
        }

        void image_EventErrorMessage(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = sender as string;
            ErrorAlert(msg);
            Logger.Info("Completed");
        }

        private bool CheckFilterDataExists(FilterDetailData filterDetailData)
        {
            foreach (UIElement item in GridPopUpPanel.Children)
            {
                if (item is ImageViewUc)
                {
                    ImageViewUc imgView = item as ImageViewUc;
                    if (imgView.filterDetailData.Equals(filterDetailData))
                    {
                        ivRenTrsfX = new Random().Next(350, 600);
                        ivRenTrsfY = new Random().Next(0, 100);
                        imgView.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsfX, ivRenTrsfY);
                        imgView.BringIntoView();
                        Panel.SetZIndex(imgView, SetZIndexValue());
                        return false;
                    }
                }
            }
            return true;
        }
        void image_EventCloseImageViewUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ImageViewUc image = (ImageViewUc)sender;
            GridPopUpPanel.Children.Remove(image);
            Logger.Info("Completed");
        }
        #endregion

        #region FloatingButton

        private void btnFloatingButton_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (!IsFloatingBtnSelected)
            {
                IsFloatingBtnSelected = true;
                FloatingButtonsUc fButtons = new FloatingButtonsUc();
                fButtons.BringIntoView();
                Panel.SetZIndex(fButtons, SetZIndexValue());
                GridFloatingContentPanel.Children.Add(fButtons);
                fButtons.EventCloseBrdFloatingUc += fButtons_EventCloseBrdFloatingUc;
                fButtons.EventContactUc += fButtons_EventContactUc;
                fButtons.EventEnquiryUc += fButtons_EventEnquiryUc;
                fButtons.EventFinanceUc += fButtons_EventFinanceUc;
                fButtons.EventFeedbackUc += fButtons_EventFeedbackUc;
                fButtons.EventScheduleTestDriveUc += fButtons_EventScheduleTestDriveUc;
                Storyboard RotatePlusButton_SB = this.TryFindResource("RotatePlusButton_SB") as Storyboard;
                RotatePlusButton_SB.Begin();
            }
            else
            {
                CloseFloatingButtonUC();
            }
            Logger.Info("Completed");
        }

        void fButtons_EventFeedbackUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseFloatingButtonUC();
            Feedback feedback = new Feedback();
            feedback.BringIntoView();
            Panel.SetZIndex(feedback, SetZIndexValue());
            GridFloatingPopUpPanel.Children.Add(feedback);
            feedback.EventCloseFeedback += feedback_EventCloseFeedback;
            feedback.EventFeedbackSubmit += feedback_EventFeedbackSubmit;
            Logger.Info("Completed");
        }

        void feedback_EventFeedbackSubmit(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = sender as string;
            SuccessAlert(msg);
            Logger.Info("Completed");
        }

        void feedback_EventCloseFeedback(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            Feedback feedback = (Feedback)sender;
            GridFloatingPopUpPanel.Children.Remove(feedback);
            Logger.Info("Completed");
        }

        #region ScheduleTestDrive
        void fButtons_EventScheduleTestDriveUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseFloatingButtonUC();
            ScheduleTestDriveUc sDrive = new ScheduleTestDriveUc();
            sDrive.gridContext.DataContext = currentProduct;
            //sDrive.gridContext.DataContext = currentSelectedCar;
            sDrive.BringIntoView();
            Panel.SetZIndex(sDrive, SetZIndexValue());
            GridFloatingPopUpPanel.Children.Add(sDrive);
            sDrive.EventCloseTestDriveUc += sDrive_EventCloseTestDriveUc;
            sDrive.EventSubmitTestDrive += sDrive_EventSubmitTestDrive;
            Logger.Info("Completed");
        }

        void sDrive_EventSubmitTestDrive(object sender, EventArgs e)
        {
            string msg = sender as string;
            SuccessAlert(msg);
        }

        void sDrive_EventCloseTestDriveUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridFloatingPopUpPanel.Children)
            {
                if (item is ScheduleTestDriveUc)
                {
                    lstIndexes.Add(GridFloatingPopUpPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridFloatingPopUpPanel.Children.RemoveAt(c);
            });
            Logger.Info("Completed");
        }
        #endregion

        #region Finance
        void fButtons_EventFinanceUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseFloatingButtonUC();
            FinanceUc finance = new FinanceUc();
            finance.gridContext.DataContext = currentProduct;
            //finance.gridContext.DataContext = currentSelectedCar;
            finance.BringIntoView();
            Panel.SetZIndex(finance, SetZIndexValue());
            GridFloatingPopUpPanel.Children.Add(finance);
            finance.EventCloseFinanceUc += finance_EventCloseFinanceUc;
            finance.EventFinanceSuccessAlert += finance_EventFinanceSuccessAlert;
            Logger.Info("Completed");
        }

        void finance_EventFinanceSuccessAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = sender as string;
            SuccessAlert(msg);
            Logger.Info("Completed");
        }

        void finance_EventCloseFinanceUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridFloatingPopUpPanel.Children)
            {
                if (item is FinanceUc)
                {
                    lstIndexes.Add(GridFloatingPopUpPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridFloatingPopUpPanel.Children.RemoveAt(c);
            });
            Logger.Info("Completed");
        }
        #endregion

        #region EnquiryForm
        void fButtons_EventEnquiryUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseFloatingButtonUC();
            EnquiryUc enquiry = new EnquiryUc();
            enquiry.gridContext.DataContext = currentProduct;
            //enquiry.gridContext.DataContext = currentSelectedCar;
            enquiry.BringIntoView();
            Panel.SetZIndex(enquiry, SetZIndexValue());
            GridFloatingPopUpPanel.Children.Add(enquiry);
            enquiry.EventCloseEnquiryUc += enquiry_EventCloseEnquiryUc;
            enquiry.EventEnquirySuccessAlert += enquiry_EventEnquirySuccessAlert;
            Logger.Info("Completed");
        }

        void enquiry_EventEnquirySuccessAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = sender as string;
            SuccessAlert(msg);
            Logger.Info("Completed");
        }

        void enquiry_EventCloseEnquiryUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridFloatingPopUpPanel.Children)
            {
                if (item is EnquiryUc)
                {
                    lstIndexes.Add(GridFloatingPopUpPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridFloatingPopUpPanel.Children.RemoveAt(c);
            });
            Logger.Info("Completed");
        }
        #endregion

        #region ContactUs
        void fButtons_EventContactUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseFloatingButtonUC();
            ContactUc contact = new ContactUc();
            contact.BringIntoView();
            Panel.SetZIndex(contact, SetZIndexValue());
            GridFloatingPopUpPanel.Children.Add(contact);
            contact.EventCloseContactUc += contact_EventCloseContactUc;
            Logger.Info("Completed");
        }

        void contact_EventCloseContactUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridFloatingPopUpPanel.Children)
            {
                if (item is ContactUc)
                {
                    lstIndexes.Add(GridFloatingPopUpPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridFloatingPopUpPanel.Children.RemoveAt(c);
            });
            Logger.Info("Completed");
        }
        #endregion

        #region AlertUC

        private void WarningAlert(string msg)
        {
            Logger.Info("Initiated");
            WarningAlertUC wAlert = new WarningAlertUC();
            wAlert.tblMessage.Text = msg;
            GridAlertPopUpPanel.Children.Add(wAlert);
            wAlert.EvntCloseWarningAlert += wAlert_EvntCloseWarningAlert;
            Logger.Info("Completed");
        }

        void wAlert_EvntCloseWarningAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            WarningAlertUC wAlert = (WarningAlertUC)sender;
            GridAlertPopUpPanel.Children.Remove(wAlert);
            Logger.Info("Completed");
        }

        private void ErrorAlert(string msg)
        {
            Logger.Info("Initiated");
            ErrorAlertUC eAlert = new ErrorAlertUC();
            eAlert.tblMessage.Text = msg;
            GridAlertPopUpPanel.Children.Add(eAlert);
            eAlert.EvntCloseErrorAlert += eAlert_EvntCloseErrorAlert;
            Logger.Info("Completed");
        }

        void eAlert_EvntCloseErrorAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ErrorAlertUC eAlert = (ErrorAlertUC)sender;
            GridAlertPopUpPanel.Children.Remove(eAlert);
            Logger.Info("Completed");
        }

        private void SuccessAlert(string msg)
        {
            Logger.Info("Initiated");
            SuccessAlertUC sAlert = new SuccessAlertUC();
            sAlert.tblMessage.Text = msg;
            GridAlertPopUpPanel.Children.Add(sAlert);
            sAlert.EventCloseSuccessMessageAlert += sAlert_EventCloseSuccessMessageAlert;
            Logger.Info("Completed");
        }

        void sAlert_EventCloseSuccessMessageAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            SuccessAlertUC sAlert = (SuccessAlertUC)sender;
            GridAlertPopUpPanel.Children.Remove(sAlert);
            Logger.Info("Completed");
        }

        private void MessageAlert(string msg)
        {
            Logger.Info("Initiated");
            MessageUc mAlert = new MessageUc();
            mAlert.tblMessage.Text = msg;
            GridAlertPopUpPanel.Children.Add(mAlert);
            mAlert.EventCloseMessageAlert += mAlert_EventCloseMessageAlert;
            Logger.Info("Completed");
        }

        void mAlert_EventCloseMessageAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            MessageUc mAlert = (MessageUc)sender;
            GridAlertPopUpPanel.Children.Remove(mAlert);
            Logger.Info("Completed");
        }
        #endregion
        void fButtons_EventCloseBrdFloatingUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseFloatingButtonUC();
            Logger.Info("Completed");
        }
        private void CloseFloatingButtonUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridFloatingContentPanel.Children)
            {
                if (item is FloatingButtonsUc)
                {
                    lstIndexes.Add(GridFloatingContentPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridFloatingContentPanel.Children.RemoveAt(c);   //remove FloatingUC by its index values
            });
            Storyboard RotateCrossButton_SB = this.TryFindResource("RotateCrossButton_SB") as Storyboard;
            RotateCrossButton_SB.Begin();
            IsFloatingBtnSelected = false;
            Logger.Info("Completed");
        }
        #endregion

        private void btnCompare_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            btnFloatingButton.Visibility = Visibility.Collapsed;
            CompareUc compare = new CompareUc();
            //compare.carImagesData = carImagesData;
            GridComparePanel.Children.Add(compare);
            compare.EventCloseCompareUc += compare_EventCloseCompareUc;
            compare.EventHomeLoad += compare_EventHomeLoad;
            compare.EventOpenCmpGalleryItem += compare_EventOpenCmpGalleryItem;
            compare.EventOpenWarningMessageAlert += compare_EventOpenWarningMessageAlert;
            compare.EventCloseCompareGalleryImages += compare_EventCloseCompareGalleryImages;
            Logger.Info("Completed");
        }

        void compare_EventCloseCompareGalleryImages(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseImageViewUC();
            Logger.Info("Completed");
        }

        void compare_EventOpenWarningMessageAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = sender as string;
            WarningAlert(msg);
            Logger.Info("Completed");
        }

        void compare_EventHomeLoad(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            LoadSlideshow();
            Logger.Info("Completed");
        }

        void compare_EventOpenCmpGalleryItem(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string strLocation = sender as string;
            //LoadImage(strLocation);
            Logger.Info("Completed");
        }
        void compare_EventCloseCompareUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            btnFloatingButton.Visibility = Visibility.Visible;
            CloseImageViewUC();
            //if (Apps.lstSelectedVariant != null)
            //{
            //    btnCompare.Content = Apps.lstSelectedVariant.Count;
            //}
            //else { btnCompare.Content = "0"; }

            CompareUc compare = (CompareUc)sender;
            GridComparePanel.Children.Remove(compare);
            Logger.Info("Completed");
        }

        private void btnAudioMute_TouchDown(object sender, TouchEventArgs e)
        {
            MEAudio.Play();
            btnAudioMute.Visibility = Visibility.Collapsed;
            btnAudioVolume.Visibility = Visibility.Visible;
        }

        private void btnAudioVolume_TouchDown(object sender, TouchEventArgs e)
        {
            MEAudio.Pause();
            btnAudioMute.Visibility = Visibility.Visible;
            btnAudioVolume.Visibility = Visibility.Collapsed;
        }

        private void MEAudio_MediaEnded(object sender, RoutedEventArgs e)
        {
            LoadAudioControl();
        }

        #region AdminLogin
        private void AdminLogin()
        {
            LoginUc login = new LoginUc();
            GridAdminPanel.Children.Add(login);
            login.EventAdminLogin += login_EventAdminLogin;
            login.EventCloseAdminLogin += login_EventCloseAdminLogin;
            login.EventWarningAlert += login_EventWarningAlert;
        }

        void login_EventWarningAlert(object sender, EventArgs e)
        {
            string msg = sender as string;
            ErrorAlert(msg);
        }
        void login_EventAdminLogin(object sender, EventArgs e)
        {
            RemoveAdminLoginControl();
            LoadAdminDashboard();
        }
        private void RemoveAdminLoginControl()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridAdminPanel.Children)
            {
                if (item is LoginUc)
                {
                    lstIndexes.Add(GridAdminPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridAdminPanel.Children.RemoveAt(c); //remove
            });
        }
        void login_EventCloseAdminLogin(object sender, EventArgs e)
        {
            LoginUc login = (LoginUc)sender;
            GridAdminPanel.Children.Remove(login);
        }
        #endregion
        #region AdminDashboard
        private void LoadAdminDashboard()
        {
            DashboardUc dashboard = new DashboardUc();
            GridAdminPanel.Children.Add(dashboard);
            dashboard.EventOpenSetAutoSync += dashboard_EventOpenSetAutoSync;
            dashboard.EventLogoutSettings += dashboard_EventLogoutSettings;
        }
        void dashboard_EventLogoutSettings(object sender, EventArgs e)
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridAdminPanel.Children)
            {
                if (item is LoginUc || item is SetAutoSyncTime || item is DashboardUc)
                {
                    lstIndexes.Add(GridAdminPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridAdminPanel.Children.RemoveAt(c); //remove
            });
        }
        private void RemoveAdminDashboard()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridAdminPanel.Children)
            {
                if (item is DashboardUc)
                {
                    lstIndexes.Add(GridAdminPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridAdminPanel.Children.RemoveAt(c); //remove
            });
        }
        #endregion
        #region AdminAutosync
        void dashboard_EventOpenSetAutoSync(object sender, EventArgs e)
        {
            RemoveAdminDashboard();
            SetAutoSyncTime autosync = new SetAutoSyncTime();
            GridAdminPanel.Children.Add(autosync);
            autosync.EventSyncNow += autosync_EventSyncNow;
            autosync.autoSyncRefresh += autosync_autoSyncRefresh;
            autosync.EventCloseSetAutoSync += autosync_EventCloseSetAutoSync;
            autosync.EventSuccessAlert += autosync_EventSuccessAlert;
            autosync.EventErrorAlert += autosync_EventErrorAlert;
        }


        void autosync_EventSyncNow(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            InitiateSync();
            Logger.Info("Completed");
        }
        void autosync_EventCloseSetAutoSync(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            SetAutoSyncTime autosync = (SetAutoSyncTime)sender;
            GridAdminPanel.Children.Remove(autosync);
            LoadAdminDashboard();
            Logger.Info("Completed");
        }
        private void InitiateSync()
        {
            Logger.Info("Initiated");
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);

                DateTime autoSyncTime = DateTime.Now.AddMilliseconds(10);

                string syncTime = autoSyncTime.ToString("HH:mm");
                if (autoSyncTimer != null)
                {
                    autoSyncTimer.Stop();
                    autoSyncTimer = null;
                }
                DateTime fromDate = DateTime.Now;
                DateTime fetchSyncDate = Convert.ToDateTime(DateTime.Today.ToShortDateString() + " " + syncTime);

                TimeSpan tspan = fetchSyncDate - fromDate;

                if (tspan.Seconds < 0)
                {
                    fetchSyncDate = Convert.ToDateTime(DateTime.Today.AddDays(1).ToShortDateString() + " " + syncTime);
                    tspan = new TimeSpan(0, 0, 1); // fetchSyncDate - fromDate;
                }
                if (autoSyncTimer == null)
                {
                    autoSyncTimer = new DispatcherTimer();
                    autoSyncTimer.Interval = new TimeSpan(Convert.ToInt32(tspan.Hours), Convert.ToInt32(tspan.Minutes), Convert.ToInt32(tspan.Seconds));
                    autoSyncTimer.Tick += autoSyncTimer_Tick;
                    autoSyncTimer.Start();
                }
                Logger.Info("Completed");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        void autoSyncTimer_Tick(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            if (!CheckAutosyncExists())
            {
                AutoSyncUC sync = new AutoSyncUC();
                GridSyncControlPanel.Children.Add(sync);
                sync.EventCloseSetAutoSync += sync_EventCloseSetAutoSync;
            }
            autoSyncTimer.Stop();
            Logger.Info("Completed");
        }
        void sync_EventCloseSetAutoSync(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            //remove autosyncuc from GridSyncControlPanel after sync
            AutoSyncUC sync = (AutoSyncUC)sender;
            GridSyncControlPanel.Children.Remove(sync);
            Logger.Info("Completed");
        }
        void autosync_autoSyncRefresh(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            InitiateAutoSync();
            Logger.Info("Completed");
        }

        private void InitiateAutoSync()
        {
            Logger.Info("Initiated");
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                if (Convert.ToBoolean(config.AppSettings.Settings["AutoSyncEnabled"].Value))
                {
                    DateTime autoSyncTime = DateTime.Now;

                    if (DateTime.TryParse(config.AppSettings.Settings["AutoSyncTiming"].Value, out autoSyncTime))
                    {
                        string syncTime = autoSyncTime.ToString("HH:mm");
                        if (autoSyncTimer != null)
                        {
                            autoSyncTimer.Stop();
                            autoSyncTimer = null;
                        }
                        DateTime fromDate = DateTime.Now;
                        DateTime fetchSyncDate = Convert.ToDateTime(DateTime.Today.ToShortDateString() + " " + syncTime);

                        TimeSpan tspan = fetchSyncDate - fromDate;

                        if (tspan.Seconds < 0)
                        {
                            fetchSyncDate = Convert.ToDateTime(DateTime.Today.AddDays(1).ToShortDateString() + " " + syncTime);
                            tspan = fetchSyncDate - fromDate;
                        }
                        if (autoSyncTimer == null)
                        {
                            autoSyncTimer = new DispatcherTimer();
                            autoSyncTimer.Interval = new TimeSpan(Convert.ToInt32(tspan.Hours), Convert.ToInt32(tspan.Minutes), Convert.ToInt32(tspan.Seconds));
                            autoSyncTimer.Tick += autoSyncTimer_Tick;
                            autoSyncTimer.Start();
                        }
                    }
                }
                Logger.Info("Completed");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }


        private bool CheckAutosyncExists()  //method to check autosyncuc already exists or not
        {
            Logger.Info("Initiated");
            foreach (UIElement item in GridSyncControlPanel.Children)
            {
                if (item is AutoSyncUC)
                {
                    Logger.Info("Completed Returns True");
                    return true;
                }
            }
            Logger.Info("Completed Returns False");
            return false;
        }

        void autosync_EventErrorAlert(object sender, EventArgs e)
        {
            string msg = sender as string;
            ErrorAlert(msg);
        }

        void autosync_EventSuccessAlert(object sender, EventArgs e)
        {
            string msg = sender as string;
            SuccessAlert(msg);
        }
        #endregion
    }
}
