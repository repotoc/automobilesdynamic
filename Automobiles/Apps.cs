﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Collections;
using System.Diagnostics;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;
using System.IO;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Reflection;
using NLog;
using TOCCatalogDB.DBFolder;

namespace Automobiles
{
    public class Apps
    {
        public static TOCCatalogDB.DBFolder.TOCCatalogDbContext tocCatalogContext;
        public static TOCCatalogDbContext myDbContext = new TOCCatalogDbContext();
        public static string strPath = AppDomain.CurrentDomain.BaseDirectory;
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        public static ObservableCollection<TOCCatalogDB.Models.Product> lstCompareProducts = new ObservableCollection<TOCCatalogDB.Models.Product>();
        //public static List<VariantItem> lstSelectedVariant { get; set; }
        
        public static bool PingCheck()
        {
            try
            {
                using (Ping ping = new Ping())
                {

                    PingReply pingStatus = ping.Send("www.google.com");

                    if (pingStatus.Status == IPStatus.Success)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static bool ValidateAdmin(string userName, string password)
        {
            bool isValid = false;
            try
            {
                string serverPath = Utilities.SlideShowServerUrl;
                string cmcLoginPath = Utilities.CmcLoginService;
                string frameURI = serverPath + "/" + cmcLoginPath + "/";
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    Uri baseAddress = new Uri(frameURI);
                    client.BaseAddress = baseAddress;
                    if (!client.DefaultRequestHeaders.Contains("Authorization"))
                    {
                        byte[] authBytes = Encoding.UTF8.GetBytes((userName + ":" + password).ToCharArray());
                        client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(authBytes));
                    }
                    //string address = "api/Admin/ValidateAdmin?userName=" + userName + "&password=" + password;
                    //string address = "api/Login?userName=" + userName + "&password=" + password + "&loginType=S";
                    
                    //HttpResponseMessage response = client.GetAsync(address).Result;
                    //if (response.StatusCode == HttpStatusCode.OK)
                    //{
                    //    isValid = response.Content.ReadAsAsync<bool>().Result;
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return isValid;
        }
    }
}
