﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NLog;
using Automobiles.Models;

namespace Automobiles
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        BackgroundWorker bgw = null;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public SplashScreen()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            bgw = new BackgroundWorker();
            bgw.WorkerReportsProgress = true;
            bgw.WorkerSupportsCancellation = true;
            bgw.DoWork += bgw_DoWork;
            bgw.ProgressChanged += bgw_ProgressChanged;
            bgw.RunWorkerCompleted += bgw_RunWorkerCompleted;
            bgw.RunWorkerAsync();
        }
        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                //Write code here to load usercontrol or launch window
                bgw = null;
                System.Threading.Thread.Sleep(1000);
                MainWindow HomePage = new MainWindow();
                HomePage.DBContext = dbContext;
                this.Hide();
                HomePage.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            tbLoadingText.Text = e.UserState as string;
        }
        TOCCatalogDB.DBFolder.TOCCatalogDbContext dbContext = null;
        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            bgw.ReportProgress(1, "Initializing Database... Please wait...");
            dbContext = new TOCCatalogDB.DBFolder.TOCCatalogDbContext();
            bgw.ReportProgress(1, "Loading Database... Please wait...");
            var database = dbContext.Category.ToList();
            Apps.tocCatalogContext = dbContext;
            bgw.ReportProgress(1, "Verifying License Information... Please wait...");
        }
    }
}
