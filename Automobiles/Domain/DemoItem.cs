﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automobiles.Domain
{
    public class DemoItem
    {
        public string Name { get; set; }

        public object Content { get; set; }
    }
}
