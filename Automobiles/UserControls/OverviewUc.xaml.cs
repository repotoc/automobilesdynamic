﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using System.Configuration;
using System.IO;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for OverviewUc.xaml
    /// </summary>
    public partial class OverviewUc : UserControl
    {
        #region variables
        public List<FilterDetail> lstFilterDetail { get; set; }
        public Product product { get; set; }
        private Logger Logger = LogManager.GetCurrentClassLogger();
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        public string carId { get; set; }
        Overview oView;
        public List<OverviewItem> lstOverviewItems { get; set; }
        public List<ImageInformation> lstImages { get; set; }
        public event EventHandler EventOpenOverviewItem;
        #endregion
        public OverviewUc()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                ICtrlOverview.ItemsSource = lstFilterDetail[0].LstFilterDetailData;

                //ICtrlOverview.ItemsSource = lstImages;

                //oView = new Overview();
                //string overViewFolder = ConfigurationManager.AppSettings["OverviewPath"];
                //string overviewPath = System.IO.Path.Combine(Apps.strPath, overViewFolder);
                //if (File.Exists(overviewPath))
                //{
                //    string strInfoData = System.IO.File.ReadAllText(overviewPath);
                //    oView = Utilities.GetOverviewData(strInfoData);
                //    if (oView != null)
                //    {
                //        ICtrlOverview.ItemsSource = oView.OverviewItem.Where(c => c.CarID.Equals(carId));
                //    }
                //}
            }
            catch (Exception ex)
            {
                Logger.Info(ex, ex.Message);
            }

        }
        private void btnOverview_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {
                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        private void svOverview_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;

        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svOverview_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.X != touchScrollEndPoint.Position.X)
            {
                isScrollMoved = true;
            }
        }

        private void svOverview_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svOverview_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svOverview_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            try
            {
                if (touchScrollStartPoint != null && touchScrollEndPoint != null)
                {
                    double diffValue = Math.Abs(touchScrollStartPoint.Position.X - touchScrollEndPoint.Position.X);
                    if (diffValue <= 10)
                    {
                        isScrollMoved = false;
                        if (btnTouchedItem != null && btnTouchedItem.DataContext != null)
                        {
                            Logger.Info("Initiated");
                            EventOpenOverviewItem(btnTouchedItem, null);
                            Logger.Info("Completed");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
    }
}
