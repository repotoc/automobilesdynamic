﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using NLog;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for CompareGalleryUc.xaml
    /// </summary>
    public partial class CompareGalleryUc : UserControl
    {
        #region variables
        public List<CompareGalleryItem> cGalleryItem { get; set; }
        public event EventHandler EventOpenCompareGalleryItem;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null; 
        #endregion

        public CompareGalleryUc()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            if (cGalleryItem != null)
            {
                ICtrlGallery.ItemsSource = cGalleryItem;
            }
            Logger.Info("Completed");
        }

        private void btnCompareGalleryItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {
                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        private void svCompareGallery_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void svCompareGallery_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svCompareGallery_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCompareGallery_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCompareGallery_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            try
            {
                if (touchScrollStartPoint != null && touchScrollEndPoint != null)
                {
                    double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                    if (diffValue <= 10)
                    {
                        isScrollMoved = false;
                        if (btnTouchedItem != null && btnTouchedItem.DataContext != null)
                        {
                            Logger.Info("Initiated");
                            CompareGalleryItem cGItem = btnTouchedItem.DataContext as CompareGalleryItem;
                            EventOpenCompareGalleryItem(cGItem.ImageLocation, null);
                            Logger.Info("Completed");                          
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
    }
}
