﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for FloatingButtonsUc.xaml
    /// </summary>
    public partial class FloatingButtonsUc : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseBrdFloatingUc;
        public event EventHandler EventFinanceUc;
        public event EventHandler EventScheduleTestDriveUc;
        public event EventHandler EventContactUc;
        public event EventHandler EventEnquiryUc;
        public event EventHandler EventCompareUc;
        public event EventHandler EventFeedbackUc;
        #endregion

        public FloatingButtonsUc()
        {
            InitializeComponent();
        }

        private void btnTestDrive_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventScheduleTestDriveUc(this, null);
            Logger.Info("Completed");
        }

        private void btnFinance_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventFinanceUc(this, null);
            Logger.Info("Completed");
        }

        private void btnContactUs_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventContactUc(this, null);
            Logger.Info("Completed");
        }

        private void brdFloatingButton_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseBrdFloatingUc(this, null);
            Logger.Info("Completed");
        }

        private void btnEnquiry_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventEnquiryUc(this, null);
            Logger.Info("Completed");
        }

        private void btnCompare_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCompareUc(this, null);
            Logger.Info("Completed");
        }

        private void btnFeedback_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventFeedbackUc(this, null);
            Logger.Info("Completed");
        }
    }
}
