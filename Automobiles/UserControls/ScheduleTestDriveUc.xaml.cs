﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using System.Windows.Media.Animation;
using NLog;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for ScheduleTestDriveUc.xaml
    /// </summary>
    public partial class ScheduleTestDriveUc : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseTestDriveUc;
        public event EventHandler EventSubmitTestDrive; 
        #endregion
        public ScheduleTestDriveUc()
        {
            InitializeComponent();
            FutureDatePicker.BlackoutDates.AddDatesInPast();
        }

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            CloseControl();
        }

        private void CloseControl()
        {
            Logger.Info("Initiated");
            Storyboard SB_Close = TryFindResource("SB_Close") as Storyboard;
            SB_Close.Completed += new EventHandler(SB_Close_Completed);
            SB_Close.Begin();
            Logger.Info("Completed");
        }

        void SB_Close_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseTestDriveUc(this, null);
            Logger.Info("Completed");
        }
        private void btnSumbit_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventSubmitTestDrive("Get ready to Drive! We will see you soon.", null);
            Logger.Info("Completed");
        }

        private void brdBackLayout_TouchDown(object sender, TouchEventArgs e)
        {
            CloseControl();
        }
    }
}
