﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using System.Configuration;
using System.IO;
using NLog;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for CompareColorsUc.xaml
    /// </summary>
    public partial class CompareColorsUc : UserControl
    {
        #region variables
        public event EventHandler EventShowSelectedCar;
        CarColors carColor;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public List<FilterDetail> lstFilterDetail { get; set; }
        #endregion
        public CompareColorsUc()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ICtrlColor.ItemsSource = lstFilterDetail;

            //string carColorFolder = ConfigurationManager.AppSettings["CarColorsPath"];
            //string path = System.IO.Path.Combine(Apps.strPath, carColorFolder);
            //if (File.Exists(path))
            //{
            //    string strInfo = System.IO.File.ReadAllText(path);
            //    carColor = Utilities.GetCarColors(strInfo);
            //    if (carColor != null)
            //    {
            //        ICtrlColor.ItemsSource = carColor.ColorItem.Where(c => c.CarID.Equals(selectedCarId));
            //    }
            //}
        }

        private void btnCarColorItem_TouchDown(object sender, TouchEventArgs e)
        {
            Button selectedCar = sender as Button;
            if (selectedCar != null)
            {
                ColorItem coloritem = selectedCar.DataContext as ColorItem;
                if (coloritem != null && EventShowSelectedCar != null)
                {
                    EventShowSelectedCar(coloritem.ImageLocation, null);
                }
            }
        }
    }
}
