﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using System.Configuration;
using System.IO;
using NLog;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for ColorsUc.xaml
    /// </summary>
    public partial class ColorsUc : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public List<FilterDetail> lstFilterDetail { get; set; }
        public event EventHandler EventSelectedColorItem;
        #endregion
        public ColorsUc()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ICtrlColor.ItemsSource = lstFilterDetail;
        }

        private void btnCarColorItem_TouchDown(object sender, TouchEventArgs e)
        {
            Button colorItem = sender as Button;
            if (colorItem != null && colorItem.DataContext != null)
            {
                EventSelectedColorItem(colorItem.DataContext, null);
            }
        }
    }
}
