﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;
using Automobiles.Models;
using System.ComponentModel;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for CarDetailsUc.xaml
    /// </summary>
    public partial class CarDetailsUc : UserControl
    {
        #region variables
        public Product pItem { get; set; }
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventAddToCompare;
        public event EventHandler EventCloseCarDetails;
        #endregion

        public CarDetailsUc()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            this.DataContext = pItem;
            Logger.Info("Completed");
        }
        private void btnAddToCompare_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Button btn = sender as Button;
            EventAddToCompare(btn, null);
            Logger.Info("Completed");
        }

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseCarDetails(this, null);
            Logger.Info("Completed");
        }
    }
}
