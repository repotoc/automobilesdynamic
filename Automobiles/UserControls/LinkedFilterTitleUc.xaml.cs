﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;
using System.Configuration;
using System.IO;
using System.Windows.Media.Animation;
using TOCCatalogDB.Models;
using Microsoft.Expression.Drawing;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for LinkedFilterTitleUc.xaml
    /// </summary>
    public partial class LinkedFilterTitleUc : UserControl
    {
        public FilterDetail filterDetail { get; set; }
        public bool isFilterOpen { get; set; }
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventLFTOverviewControl;
        public event EventHandler EventLFTBrochureControl;
        public event EventHandler EventLFTVariantsControl;
        public event EventHandler EventLFTGalleryControl;
        public LinkedFilterTitleUc()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            try
            {
                filterContext.ItemsSource = filterDetail.LstLinkedFilterTitle.ToList();
                isFilterOpen = true;
                filterLayoutPath.Start = GetStartValue(filterDetail.LstLinkedFilterTitle.Count);
                OpenMenuStoryboard();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            Logger.Info("Completed");
        }

        private Double GetStartValue(int p)
        {
            Double sPoint = 0.1;
            switch (p)
            {
                case 1: sPoint = Convert.ToDouble(0.1); break;
                case 2: sPoint = Convert.ToDouble(0.1); break;
                case 3: sPoint = Convert.ToDouble(0.2); break;
                case 4: sPoint = Convert.ToDouble(0.14); break;
                case 5: sPoint = Convert.ToDouble(0.1); break;
                default:
                    break;
            }
            return sPoint;
        }

        private void btnFilterItem_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Button btn = sender as Button;
            if (btn != null && btn.DataContext != null)
            {
                LinkedFilterTitle filterTitle = btn.DataContext as LinkedFilterTitle;
                switch (filterTitle.Name)
                {
                    case "Overview": EventLFTOverviewControl(filterTitle, null); break;
                    case "Brochure": EventLFTBrochureControl(filterTitle, null); break;
                    case "Variant": EventLFTVariantsControl(filterTitle, null); break;
                    case "Gallery": EventLFTGalleryControl(filterTitle, null); break;
                    default: break;
                }
            }
            Logger.Info("Completed");
        }
        private void btnMap_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (isFilterOpen) //Storyboard to run if menu is open
            {
                Storyboard RotateReverseCategoryIcon = TryFindResource("RotateReverseCategoryIcon") as Storyboard;
                RotateReverseCategoryIcon.Begin();
                CloseMenuStoryboard();
            }
            else if (!isFilterOpen) //Storyboard to run if menu is closed
            {
                Storyboard RotateCategoryIcon = TryFindResource("RotateCategoryIcon") as Storyboard;
                RotateCategoryIcon.Begin();
                OpenMenuStoryboard();
            }
        }

        private void OpenMenuStoryboard()
        {
            Storyboard Sb_OpenFilter = TryFindResource("Sb_OpenFilter") as Storyboard;
            Sb_OpenFilter.Completed += new EventHandler(Sb_OpenFilter_Completed);
            Sb_OpenFilter.Begin();
        }

        private void CloseMenuStoryboard()
        {
            Storyboard Sb_CloseFilter = TryFindResource("Sb_CloseFilter") as Storyboard;
            Sb_CloseFilter.Completed += new EventHandler(Sb_CloseFilter_Completed);
            Sb_CloseFilter.Begin();
        }
        void Sb_OpenFilter_Completed(object sender, EventArgs e)
        {
            isFilterOpen = true;
        }
        void Sb_CloseFilter_Completed(object sender, EventArgs e)
        {
            isFilterOpen = false;
        }
    }
}
