﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using System.Configuration;
using System.IO;
using NLog;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for VideoSlideshowUc.xaml
    /// </summary>
    public partial class VideoSlideshowUc : UserControl
    {
        #region variables
        VideoSlideshow videoData;
        private TouchPoint TouchStart;
        int videoCount, currentIndex = 0;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseVideoSlideshow;
        public event EventHandler EventOpenAdminLogin; 
        #endregion

        public VideoSlideshowUc()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //string folder = ConfigurationManager.AppSettings["VideoSlideshowPath"];
            //string path = System.IO.Path.Combine(Apps.strPath, folder);
            //if (File.Exists(path))
            //{
            //    string strInfo = System.IO.File.ReadAllText(path);
            //    videoData = Utilities.GetVideoSlideshow(strInfo);
            //    videoCount = videoData.VideoItem.Count;
            //    if (videoCount >= 1)
            //    {
            //        LoadVideo(currentIndex);
            //    }
            //}
        }

        private void LoadVideo(int currentIndex)
        {
            if (currentIndex < videoCount)
            {
                MESlideshow.Source = new Uri(videoData.VideoItem[currentIndex].VideoLocation);
                MESlideshow.Position = new TimeSpan(0, 0, 2);
                MESlideshow.Play();
            }
            else
            {
                LoadVideo(0);
            }
        }

        private void MESlideshow_MediaEnded(object sender, RoutedEventArgs e)
        {
            currentIndex++;
            LoadVideo(currentIndex);
        }
        
        private void MainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            TouchStart = e.GetTouchPoint(this);
        }
        void MainLayout_TouchMove(object sender, TouchEventArgs e)
        {
            var Touch = e.GetTouchPoint(this);
            if (TouchStart != null && TouchStart.Position.X > 1200 && Touch.Position.X > 1000)
            {
                if (Touch.Position.X < (TouchStart.Position.X - 50))
                {
                    Logger.Info("Initiated");
                    EventOpenAdminLogin(this, null);
                    Logger.Info("Completed");
                }
            }
            e.Handled = true;
        }

        private void Grid_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            MESlideshow.Stop();
            EventCloseVideoSlideshow(this, null);
            Logger.Info("Completed");
        }
    }
}
