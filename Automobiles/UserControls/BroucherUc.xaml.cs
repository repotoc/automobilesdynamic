﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;
using Automobiles.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for BroucherUc.xaml
    /// </summary>
    public partial class BroucherUc : UserControl
    {
        CarBroucher broucher;
        public string selectedCarId { get; set; }
        int totalImages = 0, currentIndex = 0;
        BroucherItem broucherItem;
        public BroucherUc()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            totalImages = 0;
            string carBroucherFolder = ConfigurationManager.AppSettings["CarBroucherPath"];
            string carBroucherpath = System.IO.Path.Combine(Apps.strPath, carBroucherFolder);
            if (File.Exists(carBroucherpath))
            {
                string strInfo = System.IO.File.ReadAllText(carBroucherpath);
                broucher = Utilities.GetCarBroucher(strInfo);
                broucherItem = broucher.BroucherItem.Where(c => c.CarID.Equals(selectedCarId)).FirstOrDefault();
                if (broucherItem != null)
                {
                    totalImages = broucherItem.ImageLocation.Count;
                    imgCarName.Source = new BitmapImage(new Uri(broucherItem.NameImageLocation));
                    tblkIndex.Text = currentIndex + 1 + "/" + totalImages;
                    if (totalImages > 0)
                    {
                        LoadImageByIndex(0);
                    }
                }
            }
        }

        private void LoadImageByIndex(int index)
        {
            tblkIndex.Text = index + 1 + "/" + totalImages;
            ImgBroucher.Source = new BitmapImage(new Uri(broucherItem.ImageLocation[index]));
        }

        private void btnPrevious_TouchDown(object sender, TouchEventArgs e)
        {
            currentIndex--;
            if (currentIndex >= 0)
            {
                LoadImageByIndex(currentIndex);
            }
            else
            {
                currentIndex = 0;
            }
        }

        private void btnNext_TouchDown(object sender, TouchEventArgs e)
        {
            currentIndex++;

            if (currentIndex < totalImages)
            {
                LoadImageByIndex(currentIndex);
            }
            else
            {
                currentIndex = totalImages - 1;
            }
        }
        public event EventHandler EventCloseBroucher;
        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            EventCloseBroucher(this,null);
        }
    }
}
