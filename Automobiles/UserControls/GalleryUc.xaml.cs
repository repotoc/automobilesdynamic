﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using NLog;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for GalleryUc.xaml
    /// </summary>
    public partial class GalleryUc : UserControl
    {
        public List<GalleryItem> gItem { get; set; }
        public List<FilterDetail> lstFilterDetail { get; set; }
        FilterDetail exteriorFilterDetails, interiorFilterDetails;
        public event EventHandler EventOpenGalleryImages;
        private Logger Logger = LogManager.GetCurrentClassLogger();


        public event EventHandler EventOpenExteriorImages;
        public event EventHandler EventOpenInteriorImages;
        public event EventHandler EventCloseImageControl;
        public GalleryUc()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            LoadGalleryImagesByMode("Exterior Gallery");
            Logger.Info("Completed");
        }

        private void LoadGalleryImagesByMode(string galleryMode)
        {
            if (lstFilterDetail != null)
            {
                Logger.Info("Initiated");
                exteriorFilterDetails = lstFilterDetail.ToList().Where(c => c.Name.Equals(galleryMode)).FirstOrDefault();
                if (exteriorFilterDetails != null && exteriorFilterDetails.LstFilterDetailData != null && exteriorFilterDetails.LstFilterDetailData.Count > 0)
                {
                    EventOpenGalleryImages(exteriorFilterDetails.LstFilterDetailData, null);
                }
                Logger.Info("Completed");
            }
        }
        private void btnExterior_TouchDown(object sender, TouchEventArgs e)
        {
            EventCloseImageControl(this, null);
            Grid.SetColumn(RectHighlight, 0);
            LoadGalleryImagesByMode("Exterior Gallery");
        }

        private void btnInterior_TouchDown(object sender, TouchEventArgs e)
        {
            EventCloseImageControl(this, null);
            Grid.SetColumn(RectHighlight, 1);
            LoadGalleryImagesByMode("Interior Gallery");
        }
    }
}
