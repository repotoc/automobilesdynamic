﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using System.Windows.Media.Animation;
using NLog;
using System.Configuration;
using System.IO;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for VariantsUc.xaml
    /// </summary>
    public partial class VariantsUc : UserControl
    {
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        public string selectedCarId { get; set; }
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventOpenItemDetails;
        CarVariant carVariant;
        public List<FilterDetail> lstFilterDetail { get; set; }
        List<FilterDetail> filterPetrol;
        List<FilterDetail> filterDiesel;
        string activeTab= string.Empty;
        public VariantsUc()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (lstFilterDetail != null && lstFilterDetail.Count > 0)
            {
                filterPetrol = lstFilterDetail.ToList().Where(c => c.Name.Equals("Petrol")).ToList();
                filterDiesel= lstFilterDetail.ToList().Where(c => c.Name.Equals("Diesel")).ToList();
                btnPetrolNotification.Content = filterPetrol[0].LstProduct.Count;
                btnDieselNotification.Content = filterDiesel[0].LstProduct.Count;
                FuelTypeSelection(180, 360, "Petrol", filterPetrol);
            }
        }

        private void btnPetrolVariant_TouchDown(object sender, TouchEventArgs e)
        {
            if (activeTab != "Petrol")
            {
                activeTab = "Petrol";
                FuelTypeSelection(180, 360, "Petrol", filterPetrol);
            }
        }
        private void btnDieselVariant_TouchDown(object sender, TouchEventArgs e)
        {
            if (activeTab != "Diesel")
            {
                activeTab = "Diesel";
                FuelTypeSelection(0, 180, "Diesel", filterDiesel);
            }
        }
        private void FuelTypeSelection(int from, int to, string fuelType, List<FilterDetail> filterList)
        {
            Storyboard SB = TryFindResource("sb_rotate") as Storyboard;
            var anim = (DoubleAnimation)SB.Children[0];
            anim.From = from;
            anim.To = to;
            SB.Begin();
            if (filterList != null)
            {
                ICtrlCarTypes.ItemsSource = filterList[0].LstProduct;
            }
        }
        private void btnCarTypeItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {
                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        private void svCarVariats_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void svCarVariats_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.X != touchScrollEndPoint.Position.X)
            {
                isScrollMoved = true;
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svCarVariats_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCarVariats_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCarVariats_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            try
            {
                if (touchScrollStartPoint != null && touchScrollEndPoint != null)
                {
                    double diffValue = Math.Abs(touchScrollStartPoint.Position.X - touchScrollEndPoint.Position.X);
                    if (diffValue <= 15)
                    {
                        isScrollMoved = false;
                        if (btnTouchedItem != null && btnTouchedItem.DataContext != null)
                        {
                            Logger.Info("Initiated");
                            Product product = btnTouchedItem.DataContext as Product;
                            if (product != null)
                            {
                                EventOpenItemDetails(product, null);
                            }
                            Logger.Info("Completed");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
    }
}
