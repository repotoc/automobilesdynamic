﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using NLog;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for BrochureUc.xaml
    /// </summary>
    public partial class BrochureUc : UserControl
    {
        #region variables
        CarBroucher carBrochureData;
        List<BroucherItem> bItemData;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventOpenBrochureItem;
        public string carID { get; set; }
        #endregion
        public BrochureUc()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            //carBrochureData = new CarBroucher();
            //string CarBrochureFolder = ConfigurationManager.AppSettings["CarBroucherPath"];
            //string carBrochurepath = System.IO.Path.Combine(Apps.strPath, CarBrochureFolder);
            //if (File.Exists(carBrochurepath))
            //{
            //    string strInfo = System.IO.File.ReadAllText(carBrochurepath);
            //    carBrochureData = Utilities.GetCarBroucher(strInfo);
            //    if (carBrochureData != null)
            //    {
            //        bItemData = new List<BroucherItem>();
            //        if (!string.IsNullOrEmpty(carID))
            //        {
            //            bItemData = carBrochureData.BroucherItem.Where(c => c.CarID.Equals(carID)).ToList();
            //            foreach (BroucherItem item in bItemData)
            //            {
            //                switch (item.ID)
            //                {
            //                    case "1":
            //                        tblkNameOne.Text = item.Name;
            //                        tblkDescOne.Text = item.Description;
            //                        imgOne.Source = new BitmapImage(new Uri(item.ImageLocation));
            //                        break;
            //                    case "2":
            //                        tblkNameTwo.Text = item.Name;
            //                        tblkDescTwo.Text = item.Description;
            //                        imgTwo.Source = new BitmapImage(new Uri(item.ImageLocation));
            //                        break;
            //                    case "3":
            //                        tblkNameThree.Text = item.Name;
            //                        tblkDescThree.Text = item.Description;
            //                        imgThree.Source = new BitmapImage(new Uri(item.ImageLocation));
            //                        break;
            //                    case "4":
            //                        tblkNameFour.Text = item.Name;
            //                        tblkDescFour.Text = item.Description;
            //                        imgFour.Source = new BitmapImage(new Uri(item.ImageLocation));
            //                        break;
            //                    default:
            //                        break;
            //                }
            //            }
            //        }
            //    }
            //}
            Logger.Info("Completed");
        }

        private void img_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Image imgPath = sender as Image;
            if (bItemData != null)
            {
                foreach (BroucherItem item in bItemData)
                {
                    if (item.ImageLocation.Equals(imgPath.Source.ToString()))
                    {
                        EventOpenBrochureItem(item.PopUpLocation, null);
                    }
                }
            }
            Logger.Info("Completed");       
        }
    }
}
