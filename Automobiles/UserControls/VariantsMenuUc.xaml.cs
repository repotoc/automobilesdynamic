﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using NLog;
using System.Configuration;
using System.IO;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for VariantsMenuUc.xaml
    /// </summary>
    public partial class VariantsMenuUc : UserControl
    {
        public VariantsMenuUc()
        {
            InitializeComponent();
        }
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        public string selectedCarId { get; set; }
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventOpenItemDetails;
        public List<FilterDetail> lstFilterDetail { get; set; }

        string activeTab = string.Empty;

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (lstFilterDetail != null && lstFilterDetail.Count > 0)
            {
                lstFilterDetail = lstFilterDetail.ToList().Where(c => c.LstProduct.Count > 0).ToList();
                ICtrlVariantTypes.ItemsSource = lstFilterDetail;
                ICtrlProducts.ItemsSource = lstFilterDetail[0].LstProduct;
            }
        }

        private void btnVariantTypeItem_TouchDown(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            FilterDetail selectedFilterDetail = btn.DataContext as FilterDetail;
            ICtrlProducts.ItemsSource = selectedFilterDetail.LstProduct;
        }

        private void btnProductItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {
                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        private void svProduct_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void svProduct_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.X != touchScrollEndPoint.Position.X)
            {
                isScrollMoved = true;
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svProduct_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svProduct_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svProduct_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            try
            {
                if (touchScrollStartPoint != null && touchScrollEndPoint != null)
                {
                    double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                    if (diffValue <= 10)
                    {
                        isScrollMoved = false;
                        if (btnTouchedItem != null && btnTouchedItem.DataContext != null)
                        {
                            Logger.Info("Initiated");
                            Product product = btnTouchedItem.DataContext as Product;
                            if (product != null)
                            {
                                EventOpenItemDetails(product, null);
                            }
                            Logger.Info("Completed");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
    }
}
