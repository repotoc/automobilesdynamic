﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for Feedback.xaml
    /// </summary>
    public partial class Feedback : UserControl
    {
        public Feedback()
        {
            InitializeComponent();
        }
        public event EventHandler EventCloseFeedback;
        public Image imgTemp;
        public Image imgTemp1;
        private void btnSmiley1_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            grdSmileyResult1.Children.Clear();
            imgTemp = new Image();
            imgTemp1 = btn.Content as Image;
            if (imgTemp1 != null)
            {
                imgTemp.Source = imgTemp1.Source;
                grdSmileyResult1.Children.Add(imgTemp);
            }
        }

        private void btnSmiley2_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            grdSmileyResult2.Children.Clear();
            imgTemp = new Image();
            imgTemp1 = btn.Content as Image;
            if (imgTemp1 != null)
            {
                imgTemp.Source = imgTemp1.Source;
                grdSmileyResult2.Children.Add(imgTemp);
            }
        }

        private void btnSmiley3_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            grdSmileyResult3.Children.Clear();
            imgTemp = new Image();
            imgTemp1 = btn.Content as Image;
            if (imgTemp1 != null)
            {
                imgTemp.Source = imgTemp1.Source;
                grdSmileyResult3.Children.Add(imgTemp);
            }
        }

        private void btnSmiley4_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            grdSmileyResult4.Children.Clear();
            imgTemp = new Image();
            imgTemp1 = btn.Content as Image;
            if (imgTemp1 != null)
            {
                imgTemp.Source = imgTemp1.Source;
                grdSmileyResult4.Children.Add(imgTemp);
            }
        }

        private void btnSmiley5_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            grdSmileyResult5.Children.Clear();
            imgTemp = new Image();
            imgTemp1 = btn.Content as Image;
            if (imgTemp1 != null)
            {
                imgTemp.Source = imgTemp1.Source;
                grdSmileyResult5.Children.Add(imgTemp);
            }
        }

        private void btnCloseFeedback_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EventCloseFeedback(this, null);
        }
        public event EventHandler EventFeedbackSubmit;
        private void btnSubmit_TouchDown(object sender, TouchEventArgs e)
        {
            grdSmileyResult1.Children.Clear();
            grdSmileyResult2.Children.Clear();
            grdSmileyResult3.Children.Clear();
            grdSmileyResult4.Children.Clear();
            grdSmileyResult5.Children.Clear();
            txtName.Text = string.Empty;
            string msg = "Thanks for your feedback !!!";
            EventFeedbackSubmit(msg, null);
        }
    }
}
