﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using NLog;
using System.Configuration;
using System.IO;
using System.Windows.Media.Animation;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for ProductColorsUc.xaml
    /// </summary>
    public partial class ProductColorsUc : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        public event EventHandler EventOpenFilterDetailItem;
        public List<FilterDetail> lstColorFilterDetail { get; set; }
        #endregion
        public ProductColorsUc()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            try
            {
                ICtrlCarMenu.ItemsSource = lstColorFilterDetail;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            Logger.Info("Completed");
        }

        private void btnMenuItem_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {
                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            Logger.Info("Completed");
        }

        private void svCarMenu_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.X != touchScrollEndPoint.Position.X)
            {
                isScrollMoved = true;
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svCarMenu_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCarMenu_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCarMenu_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            try
            {
                if (touchScrollStartPoint != null && touchScrollEndPoint != null)
                {
                    double diffValue = Math.Abs(touchScrollStartPoint.Position.X - touchScrollEndPoint.Position.X);
                    if (diffValue <= 10)
                    {
                        isScrollMoved = false;
                        if (btnTouchedItem != null && btnTouchedItem.DataContext != null)
                        {
                            FilterDetail selectedColorProduct = btnTouchedItem.DataContext as FilterDetail;
                            if (selectedColorProduct != null)
                            {
                                EventOpenFilterDetailItem(selectedColorProduct, null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        private void svCarMenu_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
