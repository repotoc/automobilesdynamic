﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using NLog;
using System.Configuration;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for SlideShowUc.xaml
    /// </summary>
    public partial class SlideShowUc : UserControl
    {
        #region Variables
        private DispatcherTimer timerImageChange;
        private Image[] ImageControls;
        private List<ImageSource> Images = new List<ImageSource>();
        private static string[] ValidImageExtensions = new[] { ".png", ".jpg", ".jpeg" };//, ".jpg", ".jpeg", ".bmp", ".gif" };
        private static string[] TransitionEffects = new[] { "Fade" };
        private string TransitionType, strImagePath = "";
        private int CurrentSourceIndex, CurrentCtrlIndex, EffectIndex = 0, IntervalTimer = 1;
        public event EventHandler EventCloseSlideshowUc;
        Ellipse[] ellipsePoints;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        #endregion

        public SlideShowUc()
        {
            InitializeComponent();

            //Initialize Image control, Image directory path and Image timer.
            IntervalTimer = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalTime"]);
            strImagePath = ConfigurationManager.AppSettings["ImagePath"];
            ImageControls = new[] { myImage, myImage2 };

            LoadImageFolder(strImagePath);

            timerImageChange = new DispatcherTimer();
            timerImageChange.Interval = new TimeSpan(0, 0, IntervalTimer);
            timerImageChange.Tick += new EventHandler(timerImageChange_Tick);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            PlaySlideShow();
            timerImageChange.IsEnabled = true;
        }
        private void LoadImageFolder(string folder)
        {
            ErrorText.Visibility = Visibility.Collapsed;
            var sw = System.Diagnostics.Stopwatch.StartNew();
            if (!System.IO.Path.IsPathRooted(folder))
                folder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, folder); //System.IO.Path.Combine(Environment.CurrentDirectory, folder);
            if (!System.IO.Directory.Exists(folder))
            {
                ErrorText.Text = "Please Upload Home Slide Images to display "; // + Environment.NewLine + folder;
                ErrorText.Visibility = Visibility.Visible;
                return;
            }
            Random r = new Random();
            var sources = from file in new System.IO.DirectoryInfo(folder).GetFiles().AsParallel()
                          where ValidImageExtensions.Contains(file.Extension, StringComparer.InvariantCultureIgnoreCase)
                          orderby r.Next()
                          select CreateImageSource(file.FullName, true);
            Images.Clear();
            Images.AddRange(sources);
            sw.Stop();
            Console.WriteLine("Total time to load {0} images: {1}ms", Images.Count, sw.ElapsedMilliseconds);
        }

        private static ImageSource CreateImageSource(string file, bool forcePreLoad)
        {
            if (forcePreLoad)
            {
                var src = new BitmapImage();
                src.BeginInit();
                src.UriSource = new Uri(file, UriKind.Absolute);
                src.CacheOption = BitmapCacheOption.OnLoad;
                src.EndInit();
                src.Freeze();
                return src;
            }
            else
            {
                var src = new BitmapImage(new Uri(file, UriKind.Absolute));
                src.Freeze();
                return src;
            }
        }

        private void timerImageChange_Tick(object sender, EventArgs e)
        {
            PlaySlideShow();
        }

        private void PlaySlideShow()
        {
            try
            {
                if (Images.Count == 0)
                {
                    return;
                }
                var oldCtrlIndex = CurrentCtrlIndex;
                CurrentCtrlIndex = (CurrentCtrlIndex + 1) % 2;
                CurrentSourceIndex = (CurrentSourceIndex + 1) % Images.Count;

                Image imgFadeOut = ImageControls[oldCtrlIndex];
                Image imgFadeIn = ImageControls[CurrentCtrlIndex];
                if (ellipsePoints != null)
                {
                    HighlightSlideshowIndicators(CurrentSourceIndex);
                }
                else
                {
                    ellipsePoints = new Ellipse[Images.Count];
                    for (int i = 0; i < Images.Count; i++)
                    {
                        ellipsePoints[i] = new Ellipse() { Width = 10, Height = 10, Margin = new Thickness(5.0), Stroke = Brushes.Red, StrokeThickness = 1 };
                        stkEllipseButtons.Children.Add(ellipsePoints[i]);
                    }
                    HighlightSlideshowIndicators(CurrentSourceIndex);
                }

                ImageSource newSource = Images[CurrentSourceIndex];
                imgFadeIn.Source = newSource;

                TransitionType = TransitionEffects[EffectIndex].ToString();

                Storyboard StboardFadeOut = (Resources[string.Format("{0}Out", TransitionType.ToString(CultureInfo.CurrentUICulture))] as Storyboard).Clone();
                StboardFadeOut.Begin(imgFadeOut);
                Storyboard StboardFadeIn = Resources[string.Format("{0}In", TransitionType.ToString(CultureInfo.CurrentUICulture))] as Storyboard;
                StboardFadeIn.Begin(imgFadeIn);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        private void HighlightSlideshowIndicators(int oldCtrlIndex)
        {
            ellipsePoints[oldCtrlIndex].Fill = new SolidColorBrush(Colors.Red);
            for (int i = 0; i < Images.Count; i++)
            {
                if (i != oldCtrlIndex)
                {
                    ellipsePoints[i].Fill = new SolidColorBrush(Colors.Transparent);
                }
            }
        }

        private void btnTMH_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            timerImageChange.Stop();
            timerImageChange = null;
            EventCloseSlideshowUc(this, null);
        }

        private TouchPoint TouchStart;
        public event EventHandler EventOpenAdminLogin;
        private void MainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            TouchStart = e.GetTouchPoint(this);
        }
        void MainLayout_TouchMove(object sender, TouchEventArgs e)
        {
            var Touch = e.GetTouchPoint(this);
            if (TouchStart != null && TouchStart.Position.X > 1000 && Touch.Position.X > 1000)
            {
                if (Touch.Position.X < (TouchStart.Position.X - 50))
                {
                    Logger.Info("Initiated");
                    EventOpenAdminLogin(this, null);
                    Logger.Info("Completed");
                }
            }
            e.Handled = true;
        }
    }
}
