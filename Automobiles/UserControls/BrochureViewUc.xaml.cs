﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for BrochureViewUc.xaml
    /// </summary>
    public partial class BrochureViewUc : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseBrochureView;
        public List<LinkedFilterDetailData> lstLnkdFilterDetailData { get; set; }
        int currentIndex = 1; 
        #endregion
        public BrochureViewUc()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            if (lstLnkdFilterDetailData != null && lstLnkdFilterDetailData.Count > 0)
            {
                LoadBrochureImage(currentIndex);
            }
            Logger.Info("Completed");
        }

        private void LoadBrochureImage(int index)
        {
            if (index == 1)
            {
                currentIndex = 1;
                btnPreviousPage.IsEnabled = false;
            }
            if (index == lstLnkdFilterDetailData.Count)
            {
                currentIndex = lstLnkdFilterDetailData.Count;
                btnNextPage.IsEnabled = false;
            }
            if (index >= 1 && index < lstLnkdFilterDetailData.Count)
            {
                this.DataContext = lstLnkdFilterDetailData[index - 1];
                if (index > 1) { btnPreviousPage.IsEnabled = true; }
                if (index < lstLnkdFilterDetailData.Count) { btnNextPage.IsEnabled = true; }
            }
            this.DataContext = lstLnkdFilterDetailData[currentIndex-1];
            TblkPageCount.Text = currentIndex + " / " + lstLnkdFilterDetailData.Count;
        }
        private void btnCloseBrochure_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseBrochureView(this, null);
            Logger.Info("Completed");
        }

        private void btnPreviousPage_TouchDown(object sender, TouchEventArgs e)
        {
            currentIndex--;
            LoadBrochureImage(currentIndex);
        }

        private void btnNextPage_TouchDown(object sender, TouchEventArgs e)
        {
            currentIndex++;
            LoadBrochureImage(currentIndex);
        }
    }
}
