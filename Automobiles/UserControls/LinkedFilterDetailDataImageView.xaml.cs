﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for LinkedFilterDetailDataImageView.xaml
    /// </summary>
    public partial class LinkedFilterDetailDataImageView : UserControl
    {
        #region variables
        public LinkedFilterDetailData linkedFilterDetailData { get; set; }
        public event EventHandler EventCloseLinkedFilterDetailDataImageView;
        public event EventHandler EventErrorLinkedFilterDetailDataImageView;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        #endregion
        public LinkedFilterDetailDataImageView()
        {
            InitializeComponent();
        }

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Storyboard CloseImage_SB = TryFindResource("CloseImage_SB") as Storyboard;
            CloseImage_SB.Completed += new EventHandler(CloseImage_SB_Completed);
            CloseImage_SB.Begin();
            Logger.Info("Completed");
        }

        void CloseImage_SB_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseLinkedFilterDetailDataImageView(this, null);
            Logger.Info("Completed");
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            try
            {
                this.DataContext = linkedFilterDetailData;
                //img.Source = new BitmapImage(new Uri(linkedFilterDetailData.Value));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                EventErrorLinkedFilterDetailDataImageView(ex.Message, null);
            }
            Logger.Info("Completed");
        }
    }
}
