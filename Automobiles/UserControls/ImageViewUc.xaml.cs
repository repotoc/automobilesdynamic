﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for ImageViewUc.xaml
    /// </summary>
    public partial class ImageViewUc : UserControl
    {
        #region variables
        public FilterDetailData filterDetailData { get; set; }
        public string dataItemDetails { get; set; }
        public event EventHandler EventCloseImageViewUc;
        public event EventHandler EventErrorMessage;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public string ImageLocation
        {
            get { return ImageLocation; }
            set { ImageLocation = value; }
        }
        #endregion

        public ImageViewUc()
        {
            InitializeComponent();
        }
        
        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Storyboard CloseImage_SB = TryFindResource("CloseImage_SB") as Storyboard;
            CloseImage_SB.Completed += new EventHandler(CloseImage_SB_Completed);
            CloseImage_SB.Begin();
            Logger.Info("Completed");
        }

        void CloseImage_SB_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseImageViewUc(this, null);
            Logger.Info("Completed");
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            try
            {
                img.Source = new BitmapImage(new Uri(dataItemDetails));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                EventErrorMessage(ex.Message, null);
            }            
            Logger.Info("Completed");
        }
    }
}
