﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;
using Automobiles.Models;
using System.Collections.ObjectModel;
using System.Windows.Media.Animation;
using System.Configuration;
using System.IO;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for CompareUc.xaml
    /// </summary>
    public partial class CompareUc : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseCompareUc;
        public event EventHandler EventOpenWarningMessageAlert;
        public event EventHandler EventHomeLoad;
        public event EventHandler EventCloseCompareGalleryImages;
        List<VariantItem> lstVariantItems;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        public CarImages carImagesData { get; set; }
        public bool isMiniTempalteOpen { get; set; }
        public Product leftSelectedItem { get; set; }
        public Product rightSelectedItem { get; set; }
        CompareGallery cGalleryData;
        CarFeatures carFeatureData;
        #endregion
        public CompareUc()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            isMiniTempalteOpen = false;
            if (Apps.lstCompareProducts != null && Apps.lstCompareProducts.Count > 0)
            {
                ICtrlCompareMiniCar.ItemsSource = Apps.lstCompareProducts;
            }


            //if (Apps.lstSelectedVariant != null && Apps.lstSelectedVariant.Count > 0)
            //{
            //    lstVariantItems = Apps.lstSelectedVariant;
            //    btnCompare.Content = Apps.lstSelectedVariant.Count;
            //    ICtrlCompareMiniCar.ItemsSource = lstVariantItems.ToList();
            //    if (!isMiniTempalteOpen)
            //    {
            //        Storyboard open_sb = TryFindResource("open_sb") as Storyboard;
            //        open_sb.Completed += new EventHandler(open_sb_Completed);
            //        open_sb.Begin();
            //    }

            //    string carCmpGalleryFolder = ConfigurationManager.AppSettings["CompareGalleryPath"];
            //    string path = System.IO.Path.Combine(Apps.strPath, carCmpGalleryFolder);
            //    if (File.Exists(path))
            //    {
            //        string strInfo = System.IO.File.ReadAllText(path);
            //        cGalleryData = Utilities.GetCompareGallery(strInfo);
            //    }

            //    string carFeatureFolder = ConfigurationManager.AppSettings["CarFeaturesPath"];
            //    string carFeaturePath = System.IO.Path.Combine(Apps.strPath, carFeatureFolder);
            //    if (File.Exists(carFeaturePath))
            //    {
            //        string strInfo = System.IO.File.ReadAllText(carFeaturePath);
            //        carFeatureData = Utilities.GetCarFeatures(strInfo);
            //    }
            //}
            //else
            //{
            //    btnCompare.Content = "0";
            //}
            Logger.Info("Completed");
        }
        private void btnHomeLogo_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventHomeLoad(this, null);
            Logger.Info("Completed");
        }

        private void btnSpecification_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseCompareGalleryImages(this, null);
            CloseCompareListSB();
            if (GridLeftCarImages.Children.Count > 0 && leftSelectedItem != null)
            {
                ClearLeftFilterControls();
                SpecificationsUc specs = new SpecificationsUc();
                specs.uGridSpecification.DataContext = leftSelectedItem;
                GridLeftFilterPanel.Children.Add(specs);
            }
            if (GridRightCarImages.Children.Count > 0 && rightSelectedItem != null)
            {
                ClearRightFilterControls();
                SpecificationsUc specs = new SpecificationsUc();
                specs.uGridSpecification.DataContext = rightSelectedItem;
                GridRightFilterPanel.Children.Add(specs);
            }
            Logger.Info("Completed");
        }

        #region ClearFilterPanelData
        private void ClearLeftFilterControls()
        {
            Logger.Info("Initiated");
            GridLeftGalleryFilterPanel.Children.Clear();
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridLeftFilterPanel.Children)
            {
                if (item is CompareColorsUc || item is CompareGalleryUc || item is SpecificationsUc || item is FeaturesUc)
                {
                    lstIndexes.Add(GridLeftFilterPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridLeftFilterPanel.Children.RemoveAt(c);   //remove 
            });
            Logger.Info("Completed");
        }
        private void ClearLeftCarImagesControls()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridLeftCarImages.Children)
            {
                if (item is CarUc)
                {
                    lstIndexes.Add(GridLeftCarImages.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridLeftCarImages.Children.RemoveAt(c);   //remove 
            });
            btnRemoveLeftCar.Visibility = Visibility.Collapsed;
            Logger.Info("Completed");
        }
        private void ClearRightCarImagesControls()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridRightCarImages.Children)
            {
                if (item is CarUc)
                {
                    lstIndexes.Add(GridRightCarImages.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridRightCarImages.Children.RemoveAt(c);   //remove 
            });
            btnRemoveRightCar.Visibility = Visibility.Collapsed;
            Logger.Info("Completed");
        }
        private void ClearRightFilterControls()
        {
            Logger.Info("Initiated");
            GridRightGalleryFilterPanel.Children.Clear();
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridRightFilterPanel.Children)
            {
                if (item is CompareColorsUc || item is CompareGalleryUc || item is SpecificationsUc || item is FeaturesUc)
                {
                    lstIndexes.Add(GridRightFilterPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridRightFilterPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        } 
        #endregion

        private void btnColors_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseCompareGalleryImages(this, null);
            CloseCompareListSB();
            if (GridLeftCarImages.Children.Count > 0)
            {
                foreach (UIElement item in GridLeftCarImages.Children)
                {
                    if (item is CarUc)
                    {
                        ClearLeftFilterControls();
                        CarUc carItem = item as CarUc;
                        GridLeftFilterPanel.Children.Clear();
                        //ColorsUc colorControl = new ColorsUc();                        
                        CompareColorsUc colorControl = new CompareColorsUc();
                        //colorControl.selectedCarId = carItem.product.SNo;
                        GridLeftFilterPanel.Children.Add(colorControl);
                    }
                }
            }
            if (GridRightCarImages.Children.Count > 0)
            {
                foreach (UIElement item in GridRightCarImages.Children)
                {
                    if (item is CarUc)
                    {
                        ClearRightFilterControls();
                        CarUc carItem = item as CarUc;
                        GridRightFilterPanel.Children.Clear();
                        //ColorsUc colorControl = new ColorsUc();
                        CompareColorsUc colorControl = new CompareColorsUc();
                        //colorControl.selectedCarId = carItem.carItem.CarID;
                        GridRightFilterPanel.Children.Add(colorControl);
                    }
                }
            }
            Logger.Info("Completed");
        }

        private void btnGallery_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseCompareGalleryImages(this, null);
            CloseCompareListSB();
            if (GridLeftCarImages.Children.Count > 0)
            {
                foreach (UIElement item in GridLeftCarImages.Children)
                {
                    if (item is CarUc)
                    {
                        ClearLeftFilterControls();
                        CarUc carItem = item as CarUc;
                        GridLeftFilterPanel.Children.Clear();
                        GridLeftGalleryFilterPanel.Children.Clear();
                        CompareGalleryUc cGallery = new CompareGalleryUc();
                        //cGallery.cGalleryItem = cGalleryData.CompareGalleryItem.Where(c => c.CarID.Equals(carItem.carItem.CarID)).ToList();
                        GridLeftGalleryFilterPanel.Children.Add(cGallery);
                        cGallery.EventOpenCompareGalleryItem += cGallery_EventOpenCompareGalleryItem;
                    }
                }
            }
            if (GridRightCarImages.Children.Count > 0)
            {
                foreach (UIElement item in GridRightCarImages.Children)
                {
                    if (item is CarUc)
                    {
                        ClearRightFilterControls();
                        CarUc carItem = item as CarUc;
                        GridRightFilterPanel.Children.Clear();
                        GridRightGalleryFilterPanel.Children.Clear();
                        CompareGalleryUc cGallery = new CompareGalleryUc();
                        //cGallery.cGalleryItem = cGalleryData.CompareGalleryItem.Where(c => c.CarID.Equals(carItem.carItem.CarID)).ToList();
                        GridRightGalleryFilterPanel.Children.Add(cGallery);
                        cGallery.EventOpenCompareGalleryItem += cGallery_EventOpenCompareGalleryItem;
                    }
                }
            }
            Logger.Info("Completed");
        }

        public event EventHandler EventOpenCmpGalleryItem;
        void cGallery_EventOpenCompareGalleryItem(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string location = sender as string;
            EventOpenCmpGalleryItem(location, null);
            Logger.Info("Completed");
        }
        private void btnCompare_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (!isMiniTempalteOpen)
            {
                Storyboard open_sb = TryFindResource("open_sb") as Storyboard;
                open_sb.Completed += new EventHandler(open_sb_Completed);
                open_sb.Begin();
            }
            else
            {
                CloseCompareListSB();
            }
            Logger.Info("Completed");
        }

        private void CloseCompareListSB()
        {
            if (isMiniTempalteOpen)
            {
                Storyboard close_sb = TryFindResource("close_sb") as Storyboard;
                close_sb.Completed += new EventHandler(close_sb_Completed);
                close_sb.Begin();
            }
        }

        void close_sb_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            isMiniTempalteOpen = false;
            Logger.Info("Completed");
        }

        void open_sb_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            isMiniTempalteOpen = true;
            Logger.Info("Completed");
        }

        private void svCompareCarsList_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnCarItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {
                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        private void CheckCarComparePosition(Product compareItem)
        {
            Logger.Info("Initiated");

            //CarImageItem carItem = carImagesData.CarImageItem.Where(c => c.CarID.Equals(vItem.CarID)).FirstOrDefault();
            if(GridLeftCarImages.Children.Count == 0)
            {
                leftSelectedItem = compareItem;
                CarUc carControl = new CarUc();
                carControl.product = compareItem;
                tblkLeftCarName.Text = leftSelectedItem.Title;
                GridLeftCarImages.Children.Add(carControl);
                btnRemoveLeftCar.Visibility = Visibility.Visible;
            }
            else if(GridRightCarImages.Children.Count == 0)
            {
                rightSelectedItem = compareItem;
                CarUc carControl = new CarUc();
                carControl.product = compareItem;
                tblkRightCarName.Text = rightSelectedItem.Title;
                GridRightCarImages.Children.Add(carControl);
                btnRemoveRightCar.Visibility = Visibility.Visible;
            }
            else
            {
                EventOpenWarningMessageAlert("Only 2 Cars can be compared at a time.", null);
            }
            if (leftSelectedItem != null && rightSelectedItem != null)
            {
                GridHeaderButtons.Visibility = Visibility.Visible;
            }
            else
            {
                GridHeaderButtons.Visibility = Visibility.Collapsed;
            }
            Logger.Info("Completed");
        }
        private void svCompareCarsList_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svCompareCarsList_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCompareCarsList_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCompareCarsList_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            try
            {
                if (touchScrollStartPoint != null && touchScrollEndPoint != null)
                {
                    double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                    if (diffValue <= 10)
                    {
                        isScrollMoved = false;
                        if (btnTouchedItem != null && btnTouchedItem.DataContext != null)
                        {
                            Logger.Info("Initiated");
                            Product selectedCmpProduct = btnTouchedItem.DataContext as Product;
                            CheckCarComparePosition(selectedCmpProduct);
                            //VariantItem vItem = btnTouchedItem.DataContext as VariantItem;
                            //CheckCarComparePosition(vItem);
                            Logger.Info("Completed");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        private void sv1_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void sv_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (sender == sv1)
            {
                sv2.ScrollToVerticalOffset(e.VerticalOffset);
                sv2.ScrollToHorizontalOffset(e.HorizontalOffset);
                sv1.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            }
            else
            {
                sv1.ScrollToVerticalOffset(e.VerticalOffset);
                sv1.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
        }

        private void btnRemoveLeftCar_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            leftSelectedItem = null;
            ClearLeftCarImagesControls();
            ClearLeftFilterControls();
            ClearRightFilterControls();
            GridHeaderButtons.Visibility = Visibility.Collapsed;
            tblkLeftCarName.Text = string.Empty;
            Logger.Info("Completed");
            
        }

        private void btnRemoveRightCar_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            rightSelectedItem = null;
            ClearRightCarImagesControls();
            ClearRightFilterControls();
            ClearLeftFilterControls();
            GridHeaderButtons.Visibility = Visibility.Collapsed;
            tblkRightCarName.Text = string.Empty;
            Logger.Info("Completed");
        }

        private void btnBackCarUc_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseCompareUc(this, null);
            Logger.Info("Completed");
        }

        private void btnClearCompareList_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            //Apps.lstSelectedVariant = new List<VariantItem>();
            btnCompare.Content = 0;
            lstVariantItems = new List<VariantItem>();
            ICtrlCompareMiniCar.ItemsSource = null;
            Logger.Info("Completed");
        }

        private void btnEditCompareList_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            GridEditList.Visibility = Visibility.Visible;
            //ICtrlEditCompare.ItemsSource = Apps.lstSelectedVariant;
            Logger.Info("Completed");
        }

        private void svEditCompareList_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnCompareDeleteItem_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Button btn = sender as Button;
            VariantItem vItem = btn.DataContext as VariantItem;
            //Apps.lstSelectedVariant.Remove(vItem);
            ICtrlEditCompare.ItemsSource = null;
            //ICtrlEditCompare.ItemsSource = Apps.lstSelectedVariant;
            ICtrlCompareMiniCar.ItemsSource = null;
            //ICtrlCompareMiniCar.ItemsSource = Apps.lstSelectedVariant;
            //btnCompare.Content = Apps.lstSelectedVariant.Count;
            Logger.Info("Completed");
        }

        private void btnCloseEditList_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            GridEditList.Visibility = Visibility.Collapsed;
            Logger.Info("Completed");
        }

        private void btnFeatures_TouchDown(object sender, TouchEventArgs e)
        {
            //Logger.Info("Initiated");
            //CloseCompareListSB();
            //EventCloseCompareGalleryImages(this, null);
            //bool featuresInLeftPanel = true;
            //bool featuresInRightPanel = true;
            //foreach (UIElement item in GridLeftFilterPanel.Children)
            //{
            //    if (item is FeaturesUc)
            //    {
            //        featuresInLeftPanel = false;
            //    }
            //}
            //foreach (UIElement item in GridRightFilterPanel.Children)
            //{
            //    if (item is FeaturesUc)
            //    {
            //        featuresInRightPanel = false;
            //    }
            //}
            //if (GridLeftCarImages.Children.Count > 0 && leftSelectedItem != null && featuresInLeftPanel)
            //{
            //    ClearLeftFilterControls();
            //    FeaturesItem featuresData = carFeatureData.FeaturesItem.Where(c => c.Version.Equals(leftSelectedItem.Version) && c.CarID.Equals(leftSelectedItem.CarID)).FirstOrDefault();
            //    FeaturesUc features = new FeaturesUc();
            //    features.uGridFeatures.DataContext = featuresData;
            //    GridLeftFilterPanel.Children.Add(features);
            //}
            //if (GridRightCarImages.Children.Count > 0 && rightSelectedItem != null && featuresInRightPanel)
            //{
            //    ClearRightFilterControls();
            //    FeaturesItem featuresData = carFeatureData.FeaturesItem.Where(c => c.Version.Equals(rightSelectedItem.Version) && c.CarID.Equals(rightSelectedItem.CarID)).FirstOrDefault();
            //    FeaturesUc features = new FeaturesUc();
            //    features.uGridFeatures.DataContext = featuresData;
            //    GridRightFilterPanel.Children.Add(features);
            //}
            Logger.Info("Completed");
        }   
    }
}
