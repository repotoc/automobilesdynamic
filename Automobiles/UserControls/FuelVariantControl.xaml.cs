﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using System.Configuration;
using System.IO;
using NLog;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for FuelVariantControl.xaml
    /// </summary>
    public partial class FuelVariantControl : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public List<LinkedFilterDetail> lstLnkdFilterDetail { get; set; }
        public event EventHandler EventSelectedFuelVariantItem;
        #endregion
        public FuelVariantControl()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            ICtrlFuelVariant.ItemsSource = lstLnkdFilterDetail;
            Logger.Info("Completed");
        }

        private void btnFuelVariantItem_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Button fuelItem = sender as Button;
            if (fuelItem != null && fuelItem.DataContext != null)
            {
                EventSelectedFuelVariantItem(fuelItem.DataContext, null);
            }
            Logger.Info("Completed");
        }
    }
}
