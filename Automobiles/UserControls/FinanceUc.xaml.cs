﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using Automobiles.Models;
using NLog;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for FinanceUc.xaml
    /// </summary>
    public partial class FinanceUc : UserControl
    {
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseFinanceUc;
        public FinanceUc()
        {
            InitializeComponent();
        }

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            CloseControl();
        }

        private void CloseControl()
        {
            Logger.Info("Initiated");
            Storyboard SB_Close = TryFindResource("SB_Close") as Storyboard;
            SB_Close.Completed += new EventHandler(SB_Close_Completed);
            SB_Close.Begin();
            Logger.Info("Completed");
        }

        void SB_Close_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseFinanceUc(this, null);
            Logger.Info("Completed");
        }
        public event EventHandler EventFinanceSuccessAlert;
        private void btnPayNow_TouchDown(object sender, TouchEventArgs e)
        {
            EventFinanceSuccessAlert("Thank You !!!", null);
        }

        private void brdBackLayout_TouchDown(object sender, TouchEventArgs e)
        {
            CloseControl();
        }

        private void sliderRateOfIntereset_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            tblkRateofInterest.Text = sliderRateOfIntereset.Value.ToString();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tblkRateofInterest.Text = sliderRateOfIntereset.Value.ToString();
            tblkTermsofLoan.Text = sliderLoanTerms.Value.ToString();
        }

        private void sliderLoanTerms_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            tblkTermsofLoan.Text = sliderLoanTerms.Value.ToString();
        }
    }
}
