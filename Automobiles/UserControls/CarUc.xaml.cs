﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using NLog;
using System.Windows.Media.Animation;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for CarUc.xaml
    /// </summary>
    public partial class CarUc : UserControl
    {
        #region variables
        int currentImgCountValue = 0;
        protected TouchPoint TouchStart;
        public int imagesCount { get; set; }
        public Product product { get; set; }
        #endregion

        public CarUc()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (product != null)
            {
                //carImage.Source = new BitmapImage(new Uri(product.ImageLocation[0]));
                //imagesCount = carItem.ImageLocation.Count - 1;
                //tblkCarName.Text = product.Title;
                //imgCarName.Source = new BitmapImage(new Uri(carItem.NameImageLocation));

                //carImage.Source = new BitmapImage(new Uri(product.LstImages[0].ImageID));
                imagesCount = product.LstImages.Count - 1;
                tblkCarName.Text = product.Title;
            }
        }

        private void Rotate(int currentImgCountValue)
        {
            //string filename = ((currentImgCountValue < imagesCount + 1) ? carItem.ImageLocation[currentImgCountValue] : carItem.ImageLocation[currentImgCountValue]);
            string filename = ((currentImgCountValue < imagesCount + 1) ? product.LstImages[currentImgCountValue].ImageID : product.LstImages[currentImgCountValue].ImageID);
            carImage.Source = new BitmapImage(new Uri(filename));
            carImage.Stretch = Stretch.Uniform;
        }
        private void LoadPreviousImage()
        {
            currentImgCountValue++;
            if (currentImgCountValue > imagesCount)
            {
                currentImgCountValue = 1;
            }
            Rotate(currentImgCountValue);
        }
        private void LoadNextImage()
        {
            currentImgCountValue--;
            if (currentImgCountValue < 1)
            {
                currentImgCountValue = imagesCount;
            }
            Rotate(currentImgCountValue);
        }
        private void GridCarLayout_TouchDown(object sender, TouchEventArgs e)
        {
            TouchStart = e.GetTouchPoint(this);
        }

        private void GridCarLayout_TouchMove(object sender, TouchEventArgs e)
        {
            var Touch = e.GetTouchPoint(this);
            //right now a swipe is 20 pixels 
            //Swipe Left
            if (TouchStart != null && Touch.Position.X > (TouchStart.Position.X + 20))
            {
                LoadNextImage();
                TouchStart = e.GetTouchPoint(this);
            }
            //Swipe Right
            if (TouchStart != null && Touch.Position.X < (TouchStart.Position.X - 20))
            {
                LoadPreviousImage();
                TouchStart = e.GetTouchPoint(this);
            }
            e.Handled = true;
        }


        //private void Rotate(int currentImgCountValue)
        //{
        //    string filename = ((currentImgCountValue < imagesCount + 1) ? carItem.ImageLocation[currentImgCountValue] : carItem.ImageLocation[currentImgCountValue]);
        //    carImage.Source = new BitmapImage(new Uri(filename));
        //    carImage.Stretch = Stretch.Uniform;
        //}
        //private void LoadPreviousImage()
        //{
        //    currentImgCountValue++;
        //    if (currentImgCountValue > imagesCount)
        //    {
        //        currentImgCountValue = 1;
        //    }
        //    Rotate(currentImgCountValue);
        //}
        //private void LoadNextImage()
        //{
        //    currentImgCountValue--;
        //    if (currentImgCountValue < 1)
        //    {
        //        currentImgCountValue = imagesCount;
        //    }
        //    Rotate(currentImgCountValue);
        //}
        //private void GridCarLayout_TouchDown(object sender, TouchEventArgs e)
        //{
        ////    TouchStart = e.GetTouchPoint(this);
        //}

        //private void GridCarLayout_TouchMove(object sender, TouchEventArgs e)
        //{
        ////    var Touch = e.GetTouchPoint(this);
        ////    //right now a swipe is 20 pixels 
        ////    //Swipe Left
        ////    if (TouchStart != null && Touch.Position.X > (TouchStart.Position.X + 15))
        ////    {
        ////        LoadNextImage();
        ////        TouchStart = e.GetTouchPoint(this);
        ////    }
        ////    //Swipe Right
        ////    if (TouchStart != null && Touch.Position.X < (TouchStart.Position.X - 15))
        ////    {
        ////        LoadPreviousImage();
        ////        TouchStart = e.GetTouchPoint(this);
        ////    }
        ////    e.Handled = true;
        //}
    }
}
