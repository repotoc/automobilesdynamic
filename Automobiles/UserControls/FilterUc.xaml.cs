﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;
using System.Configuration;
using System.IO;
using System.Windows.Media.Animation;
using TOCCatalogDB.Models;
using Microsoft.Expression.Drawing;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for FilterUc.xaml
    /// </summary>
    public partial class FilterUc : UserControl
    {
        #region variables
        public List<FilterTitle> prodFilter { get; set; }
        FilterTitle filter = new FilterTitle();
        public bool isFilterOpen { get; set; }
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventColorControl;
        public event EventHandler EventOverviewControl;
        public event EventHandler EventBrochureControl;
        public event EventHandler EventVariantsControl;
        public event EventHandler EventGalleryControl;
        #endregion
        public FilterUc()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            try
            {
                filterContext.ItemsSource = prodFilter.ToList();
                isFilterOpen = true;
                OpenMenuStoryboard();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            Logger.Info("Completed");
        }
        
        private void btnFilterItem_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Button btn = sender as Button;
            if (btn != null && btn.DataContext != null)
            {
                filter = btn.DataContext as FilterTitle;
                switch (filter.Name)
                {
                    case "Overview": EventOverviewControl(filter.LstFilterDetail, null); break;
                    case "Brochure": EventBrochureControl(filter.LstFilterDetail, null); break;
                    case "Variant": EventVariantsControl(filter.LstFilterDetail, null); break;
                    case "Gallery": EventGalleryControl(filter.LstFilterDetail, null); break;
                    case "Colors": EventColorControl(filter.LstFilterDetail, null); break;
                    default: break;
                }
            }
            Logger.Info("Completed");
        }
        private void btnMap_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (isFilterOpen) //Storyboard to run if menu is open
            {
                Storyboard RotateReverseCategoryIcon = TryFindResource("RotateReverseCategoryIcon") as Storyboard;
                RotateReverseCategoryIcon.Begin();
                CloseMenuStoryboard();
            }
            else if (!isFilterOpen) //Storyboard to run if menu is closed
            {
                Storyboard RotateCategoryIcon = TryFindResource("RotateCategoryIcon") as Storyboard;
                RotateCategoryIcon.Begin();
                OpenMenuStoryboard();
            }
        }

        private void OpenMenuStoryboard()
        {
            Storyboard Sb_OpenFilter = TryFindResource("Sb_OpenFilter") as Storyboard;
            Sb_OpenFilter.Completed += new EventHandler(Sb_OpenFilter_Completed);
            Sb_OpenFilter.Begin();
        }

        private void CloseMenuStoryboard()
        {
            Storyboard Sb_CloseFilter = TryFindResource("Sb_CloseFilter") as Storyboard;
            Sb_CloseFilter.Completed += new EventHandler(Sb_CloseFilter_Completed);
            Sb_CloseFilter.Begin();
        }
        void Sb_OpenFilter_Completed(object sender, EventArgs e)
        {
            isFilterOpen = true;
        }
        void Sb_CloseFilter_Completed(object sender, EventArgs e)
        {
            isFilterOpen = false;
        }
    }
}
