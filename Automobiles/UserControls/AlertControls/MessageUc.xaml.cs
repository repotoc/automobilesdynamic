﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automobiles.UserControls.AlertControls
{
    /// <summary>
    /// Interaction logic for MessageUc.xaml
    /// </summary>
    public partial class MessageUc : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseMessageAlert;
        Timer delayTimer;
        #endregion
        public MessageUc()
        {
            InitializeComponent();
            delayTimer = new Timer();
            delayTimer.Interval = 3000;
            delayTimer.Elapsed += delayTimer_Elapsed;
            delayTimer.Start();
        }
        void delayTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Logger.Info("Initiated");
            this.Dispatcher.Invoke((Action)(() =>
            {
                Storyboard CloseAlert_SB = TryFindResource("CloseAlert_SB") as Storyboard;  // gets the CloseAlert_SB storyboard from resources
                CloseAlert_SB.Completed += new EventHandler(CloseAlert_SB_Completed);   //event raises when CloseAlert_SB completed
                CloseAlert_SB.Begin();    //starts CloseReviews_SB storyboard
            }));
            Logger.Info("Completed");
        }
        void CloseAlert_SB_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            try
            {
                delayTimer.Stop();
                delayTimer = null;
                EventCloseMessageAlert(this, null);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            Logger.Info("Completed");
        }
    }
}
