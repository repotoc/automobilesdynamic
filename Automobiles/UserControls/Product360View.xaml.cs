﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for Product360View.xaml
    /// </summary>
    public partial class Product360View : UserControl
    {
        #region variables
        int currentImgCountValue = 0;
        protected TouchPoint TouchStart;
        public int imagesCount { get; set; }
        public Product currentProduct { get; set; }
        List<LinkedFilterDetailData> lstFilterDetailData;
        #endregion
        public Product360View()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (currentProduct != null && currentProduct.LstLinkingFilterDetail != null && currentProduct.LstLinkingFilterDetail.Count > 0)
            {
                LinkedFilterDetail linkedFilterDetailData = currentProduct.LstLinkingFilterDetail.FirstOrDefault(c => c.LinkedFilter.Name.Equals("360 Degree"));
                if (linkedFilterDetailData != null && linkedFilterDetailData.LstLinkedFilterDetailData != null && linkedFilterDetailData.LstLinkedFilterDetailData.Count > 0)
                {
                    lstFilterDetailData = linkedFilterDetailData.LstLinkedFilterDetailData.Where(c => c.Product.ID.Equals(currentProduct.ID)).ToList();
                    if (lstFilterDetailData != null && lstFilterDetailData.Count > 0)
                    {
                        carImage.Source = new BitmapImage(new Uri(lstFilterDetailData[0].Value));
                        imagesCount = lstFilterDetailData.Count - 1;
                    }
                }
                tblkCarName.Text = currentProduct.Title;
            }
        }

        private void Rotate(int currentImgCountValue)
        {
            string filename = ((currentImgCountValue < imagesCount + 1) ? lstFilterDetailData[currentImgCountValue].Value : lstFilterDetailData[currentImgCountValue].Value);
            carImage.Source = new BitmapImage(new Uri(filename));
            carImage.Stretch = Stretch.Uniform;
        }
        private void LoadPreviousImage()
        {
            currentImgCountValue++;
            if (currentImgCountValue > imagesCount)
            {
                currentImgCountValue = 1;
            }
            Rotate(currentImgCountValue);
        }
        private void LoadNextImage()
        {
            currentImgCountValue--;
            if (currentImgCountValue < 1)
            {
                currentImgCountValue = imagesCount;
            }
            Rotate(currentImgCountValue);
        }
        private void GridCarLayout_TouchDown(object sender, TouchEventArgs e)
        {
            TouchStart = e.GetTouchPoint(this);
        }

        private void GridCarLayout_TouchMove(object sender, TouchEventArgs e)
        {
            var Touch = e.GetTouchPoint(this);
            //right now a swipe is 20 pixels 
            //Swipe Left
            if (TouchStart != null && Touch.Position.X > (TouchStart.Position.X + 20))
            {
                LoadNextImage();
                TouchStart = e.GetTouchPoint(this);
            }
            //Swipe Right
            if (TouchStart != null && Touch.Position.X < (TouchStart.Position.X - 20))
            {
                LoadPreviousImage();
                TouchStart = e.GetTouchPoint(this);
            }
            e.Handled = true;
        }
    }
}
