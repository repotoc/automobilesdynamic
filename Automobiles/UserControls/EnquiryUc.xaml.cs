﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using Automobiles.Models;
using NLog;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for EnquiryUc.xaml
    /// </summary>
    public partial class EnquiryUc : UserControl
    {
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseEnquiryUc;
        public event EventHandler EventEnquirySuccessAlert;
        public EnquiryUc()
        {
            InitializeComponent();
        }

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            CloseControl();
        }

        private void CloseControl()
        {
            Logger.Info("Initiated");
            Storyboard SB_Close = TryFindResource("SB_Close") as Storyboard;
            SB_Close.Completed += new EventHandler(SB_Close_Completed);
            SB_Close.Begin();
            Logger.Info("Completed");
        }

        void SB_Close_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");         
            EventCloseEnquiryUc(this, null);
            Logger.Info("Completed");
        }
        private void btnSubmit_TouchDown(object sender, TouchEventArgs e)
        {
            EventEnquirySuccessAlert("Thank You for your inputs, our customer service person will contact you soon...", null);
        }

        private void brdBackLayout_TouchDown(object sender, TouchEventArgs e)
        {
            CloseControl();
        }
    }
}
