﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "ColorItem")]
    public class ColorItem
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "IsSelected")]
        public string IsSelected { get; set; }
        [XmlElement(ElementName = "CarID")]
        public string CarID { get; set; }
        [XmlElement(ElementName = "ImageLocation")]
        public string ImageLocation { get; set; }
        [XmlElement(ElementName = "Thumbnail")]
        public string Thumbnail { get; set; }
        

    }

    [XmlRoot(ElementName = "CarColors")]
    public class CarColors
    {
        [XmlElement(ElementName = "ColorItem")]
        public List<ColorItem> ColorItem { get; set; }
    }
}
