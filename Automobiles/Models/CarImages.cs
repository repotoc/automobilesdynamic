﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "CarImageItem")]
    public class CarImageItem
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "IsSelected")]
        public string IsSelected { get; set; }
        [XmlElement(ElementName = "CarID")]
        public string CarID { get; set; }
        [XmlElement(ElementName = "ImageLocation")]
        public List<string> ImageLocation { get; set; }
        [XmlElement(ElementName = "NameImageLocation")]
        public string NameImageLocation { get; set; }
    }

    [XmlRoot(ElementName = "CarImages")]
    public class CarImages
    {
        [XmlElement(ElementName = "CarImageItem")]
        public List<CarImageItem> CarImageItem { get; set; }
    }
}
