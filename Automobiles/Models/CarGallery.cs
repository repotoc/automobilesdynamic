﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "GalleryItem")]
    public class GalleryItem
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "IsSelected")]
        public string IsSelected { get; set; }
        [XmlElement(ElementName = "CarID")]
        public string CarID { get; set; }
        [XmlElement(ElementName = "Type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "XAxis")]
        public string XAxis { get; set; }
        [XmlElement(ElementName = "YAxis")]
        public string YAxis { get; set; }
        [XmlElement(ElementName = "ImageLocation")]
        public string ImageLocation { get; set; }
    }

    [XmlRoot(ElementName = "CarGallery")]
    public class CarGallery
    {
        [XmlElement(ElementName = "GalleryItem")]
        public List<GalleryItem> GalleryItem { get; set; }
    }
}
