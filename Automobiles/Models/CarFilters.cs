﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "FilterItem")]
    public class FilterItem
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "IsSelected")]
        public string IsSelected { get; set; }
        [XmlElement(ElementName = "CarID")]
        public string CarID { get; set; }
        [XmlElement(ElementName = "ImageLocation")]
        public string ImageLocation { get; set; }
    }

    [XmlRoot(ElementName = "CarFilters")]
    public class CarFilters
    {
        [XmlElement(ElementName = "FilterItem")]
        public List<FilterItem> FilterItem { get; set; }
    }
}
