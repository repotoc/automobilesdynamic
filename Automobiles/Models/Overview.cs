﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "OverviewItem")]
    public class OverviewItem
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "CarID")]
        public string CarID { get; set; }
        [XmlElement(ElementName = "ImageLocation")]
        public string ImageLocation { get; set; }

        [XmlElement(ElementName = "TempLocation")]
        public string TempLocation { get; set; }
        [XmlElement(ElementName = "Destination")]
        public string Destination { get; set; }
    }

    [XmlRoot(ElementName = "Overview")]
    public class Overview
    {
        [XmlElement(ElementName = "OverviewItem")]
        public List<OverviewItem> OverviewItem { get; set; }
    }

}
