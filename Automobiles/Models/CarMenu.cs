﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "CarMenuItem")]
    public class CarMenuItem
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "IsSelected")]
        public string IsSelected { get; set; }
        [XmlElement(ElementName = "CarID")]
        public string CarID { get; set; }
        [XmlElement(ElementName = "ImageLocation")]
        public string ImageLocation { get; set; }
        [XmlElement(ElementName = "NameImageLocation")]
        public string NameImageLocation { get; set; }
    }

    [XmlRoot(ElementName = "CarMenu")]
    public class CarMenu
    {
        [XmlElement(ElementName = "CarMenuItem")]
        public List<CarMenuItem> CarMenuItem { get; set; }
    }
}
