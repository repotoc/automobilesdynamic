﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "AudioItem")]
    public class AudioItem
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "AudioLocation")]
        public string AudioLocation { get; set; }
    }

    [XmlRoot(ElementName = "Audio")]
    public class Audio
    {
        [XmlElement(ElementName = "AudioItem")]
        public AudioItem AudioItem { get; set; }
    }
}
