﻿using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automobiles.Admin
{
    /// <summary>
    /// Interaction logic for LoginUc.xaml
    /// </summary>
    public partial class LoginUc : UserControl
    {
        #region variables
        public event EventHandler EventCloseAdminLogin;
        public event EventHandler EventAdminLogin;
        public event EventHandler EventWarningAlert;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        #endregion
        public LoginUc()
        {
            InitializeComponent();
        }
        private void btnCancelLogin_TouchDown(object sender, TouchEventArgs e)
        {
            Storyboard CloseSB = TryFindResource("CloseSB") as Storyboard;
            CloseSB.Completed +=  new EventHandler(CloseSB_Completed);
            CloseSB.Begin();
        }

        void CloseSB_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseAdminLogin(this, null);
            Logger.Info("Completed");
        }

        private void btnAdminLogin_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            ValidateLogin();
            Logger.Info("Completed");
        }
        private void ValidateLogin()
        {
            Logger.Info("Initiated");
            if (!string.IsNullOrEmpty(txtName.Text) && !string.IsNullOrEmpty(txtPassword.Password))
            {
                if (Apps.PingCheck())
                {
                    SaveCredentials(txtName.Text.Trim(), txtPassword.Password.Trim());
                    EventAdminLogin(this, null);

                    //if (Apps.ValidateAdmin(txtName.Text, txtPassword.Password))
                    //{
                    //    SaveCredentials(txtName.Text.Trim(), txtPassword.Password.Trim());
                    //    EventAdminLogin(this, null);
                    //}
                    //else
                    //{
                    //    EventWarningAlert("Invalid username and password.", null);
                    //    ClearFields();
                    //}
                }
                else if (txtName.Text.Trim().Equals(Utilities.UserName) && txtPassword.Password.Trim().Equals(Utilities.Password))
                {
                    EventAdminLogin(this, null);
                }
                else
                {
                    ClearFields();
                    EventWarningAlert("Invalid username and password.", null);
                }
            }
            else
            {
                ClearFields();
                EventWarningAlert("Username and Password should not be empty.", null);
            }
            Logger.Info("Completed");
        }

        private void ClearFields()
        {
            txtName.Text = txtPassword.Password = string.Empty;
        }
        private void SaveCredentials(string userName, string password)
        {
            Logger.Info("Initiated");
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                config.AppSettings.Settings["Locationusername"].Value = userName;
                config.AppSettings.Settings["Locationpassword"].Value = password;


                RegistryKey regKey = Registry.CurrentUser.OpenSubKey("Software\\TouchOnCloud\\CMC");
                if (regKey != null)
                {
                    config.AppSettings.Settings["Username"].Value = Convert.ToString(regKey.GetValue("UserName"));
                    config.AppSettings.Settings["Password"].Value = Convert.ToString(regKey.GetValue("Password"));
                }
                else
                {
                    Logger.Info("CMC Username and Password are empty, it may interrupt HomeSlideShow and Offer Images.");
                }
                config.Save(ConfigurationSaveMode.Full, true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            Logger.Info("Completed");
        }

        private void txtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                ValidateLogin();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                ValidateLogin();
            }
        }
    }
}
